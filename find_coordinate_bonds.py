import glob
import numpy as np
import sys
import subprocess
from compare_methods import Compare

fname = "all_coor_files.txt"

com = Compare()

args = sys.argv
rarray, sarray = com.loadData(args, compare_reactants = False)

f_save = open(fname, "w+")
coors_indices = []

for index in range(len(rarray)):
    is_coor = com.checkCoordinate(rarray[index])

    if is_coor in [1,2]:
        coors_indices.append(index)

for c_i in coors_indices:
    left_rf = rarray[c_i][0]
    right_rf = rarray[c_i][1]
    f_save.write(str(left_rf) + ";" + str(right_rf) + "\n")

# subprocess.run(["python3", "toprint_explicit.py", fname])
# subprocess.run(["python3", "tolatex_explicit.py", fname])