files = []


files.append(["Ex23/a_ns_p_explicit_h.txt", "Ex23/a_ns_p_explicit_h_withname.txt"])
files.append(["Ex23/a_ns_p_explicit_h.txt.proc", "Ex23/a_ns_p_explicit_h_withname.txt"])
files.append(["Ex23/a_ns_p_explicit.txt", "Ex23/a_ns_p_explicit_withname.txt"])
files.append(["Ex23/a_ns_p_explicit.txt.proc", "Ex23/a_ns_p_explicit_withname.txt"])
files.append(["Ex23/na_ns_np_explicit_h.txt", "Ex23/na_ns_np_explicit_h_withname.txt"])
files.append(["Ex23/na_ns_np.txt", "Ex23/na_ns_np_withname.txt"])
files.append(["Ex23/na_ns_p_explicit_h.txt", "Ex23/na_ns_p_explicit_h_withname.txt"])
files.append(["Ex23/na_ns_p_explicit_h.txt.proc", "Ex23/na_ns_p_explicit_h_withname.txt"])
files.append(["Ex23/na_ns_p.txt", "Ex23/na_ns_p_withname.txt"])
files.append(["Ex23/na_ns_p.txt.proc", "Ex23/na_ns_p_withname.txt"])

for fname in files:
    with open(fname[1]) as f:
        withname_content = f.readlines()

    withname_content = [x.strip() for x in withname_content]

    names = []
    for smiles in withname_content:
        tmp = smiles.split("  ")
        tmp_name = tmp[0]
        names.append(tmp_name)

    with open(fname[0]) as f:
        woname_content = f.readlines()
    woname_content = [x.strip() for x in woname_content]
    
    if len(withname_content) != len(woname_content):
        print("Error")
        break
    
    for j in range(len(woname_content)):
        woname_content[j] = names[j]  + "  " + woname_content[j]

    fout = open(fname[0], "w+")
    for line in woname_content:
        fout.write(line + "\n")
    fout.close()