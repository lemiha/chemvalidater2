import glob

files = glob.glob("Ex25/*train*")

for fname in files:
    with open(fname) as f:
        content = f.readlines()

    content = [x.split("  ")[1] for x in content]

    fout = open(fname, "w+")
    for line in content:
        fout.write(line)
    fout.close()