import rdkit.Chem as Chem
import numpy as np
from tqdm import tqdm

'''
This script prepares the data used in Wengong Jin's NIPS paper on predicting reaction outcomes for the modified
forward prediction script. Rather than just training to predict which bonds change, we make a direct prediction
on HOW those bonds change
'''

def my_sanitize(mol):
    mol.UpdatePropertyCache()
    Chem.GetSymmSSSR(mol)
    Chem.AssignRadicals(mol)
    Chem.SetAromaticity(mol)
    Chem.SetConjugation(mol)
    Chem.SetHybridization(mol)

    return mol



def get_changed_bonds(rxn_smi):
    # reactants = Chem.MolFromSmarts(rxn_smi.split('>')[0])

    reactants = Chem.MolFromSmiles(rxn_smi.split('>')[0], sanitize = False)
    reactants = my_sanitize(reactants)

    # products  = Chem.MolFromSmarts(rxn_smi.split('>')[2])
    products = Chem.MolFromSmiles(rxn_smi.split('>')[2], sanitize = False)
    products = my_sanitize(products)

    conserved_maps = [a.GetProp('molAtomMapNumber') for a in products.GetAtoms() if a.HasProp('molAtomMapNumber')]
    bond_changes = set() # keep track of bond changes

    # Look at changed bonds
    bonds_prev = {}
    for bond in reactants.GetBonds():
        nums = sorted(
            [bond.GetBeginAtom().GetProp('molAtomMapNumber'),
             bond.GetEndAtom().GetProp('molAtomMapNumber')])
        if (nums[0] not in conserved_maps) and (nums[1] not in conserved_maps): continue
        bonds_prev['{}~{}'.format(nums[0], nums[1])] = bond.GetBondTypeAsDouble()
    bonds_new = {}
    for bond in products.GetBonds():
        nums = sorted(
            [bond.GetBeginAtom().GetProp('molAtomMapNumber'),
             bond.GetEndAtom().GetProp('molAtomMapNumber')])
        bonds_new['{}~{}'.format(nums[0], nums[1])] = bond.GetBondTypeAsDouble()


    for bond in bonds_prev:
        if bond not in bonds_new:
            bond_changes.add((bond.split('~')[0], bond.split('~')[1], 0.0)) # lost bond
        else:
            if bonds_prev[bond] != bonds_new[bond]:
                bond_changes.add((bond.split('~')[0], bond.split('~')[1], bonds_new[bond])) # changed bond
    for bond in bonds_new:
        if bond not in bonds_prev:
            bond_changes.add((bond.split('~')[0], bond.split('~')[1], bonds_new[bond]))  # new bond

    return bond_changes


def process_file(fpath):
    with open(fpath, 'r') as fid_in, open(fpath + '.proc', 'w') as fid_out:
        for line in tqdm(fid_in):
            rxn_smi = line.strip().split('  ')[1].split(" ")[0]
            bond_changes = get_changed_bonds(rxn_smi)
            fid_out.write('{} {}\n'.format(line.strip().split('  ')[0] + "  " + rxn_smi, ';'.join(['{}-{}-{}'.format(x[0], x[1], x[2]) for x in bond_changes])))
    print('Finished processing {}'.format(fpath))

if __name__ == '__main__':
    # Test summarization
    for rxn_smi in [
            # '[CH2:15]([CH:16]([CH3:17])[CH3:18])[Mg+:19].[CH2:20]1[O:21][CH2:22][CH2:23][CH2:24]1.[Cl-:14].[OH:1][c:2]1[n:3][cH:4][c:5]([C:6](=[O:7])[N:8]([O:9][CH3:10])[CH3:11])[cH:12][cH:13]1>>[OH:1][c:2]1[n:3][cH:4][c:5]([C:6](=[O:7])[CH2:15][CH:16]([CH3:17])[CH3:18])[cH:12][cH:13]1',
            # '[CH3:14][NH2:15].[N+:1](=[O:2])([O-:3])[c:4]1[cH:5][c:6]([C:7](=[O:8])[OH:9])[cH:10][cH:11][c:12]1[Cl:13].[OH2:16]>>[N+:1](=[O:2])([O-:3])[c:4]1[cH:5][c:6]([C:7](=[O:8])[OH:9])[cH:10][cH:11][c:12]1[NH:15][CH3:14]',
            # '[CH2:1]([CH3:2])[n:3]1[cH:4][c:5]([C:22](=[O:23])[OH:24])[c:6](=[O:21])[c:7]2[cH:8][c:9]([F:20])[c:10](-[c:13]3[cH:14][cH:15][c:16]([NH2:19])[cH:17][cH:18]3)[cH:11][c:12]12.[CH:25](=[O:26])[OH:27]>>[CH2:1]([CH3:2])[n:3]1[cH:4][c:5]([C:22](=[O:23])[OH:24])[c:6](=[O:21])[c:7]2[cH:8][c:9]([F:20])[c:10](-[c:13]3[cH:14][cH:15][c:16]([NH:19][CH:25]=[O:26])[cH:17][cH:18]3)[cH:11][c:12]12',
            # '[Mg++:77].[S+:1]12[Fe-:3]3[S+:2]4[Fe-:4]1[S+:8]1[Fe-:5]2[S+:7]3[Fe-:6]41.[S+:9]12[Fe-:11]3[S+:10]4[Fe-:12]1[S+:16]1[Fe-:13]2[S+:15]3[Fe-:14]41.[S+:17]12[Fe-:21]3[S+:23]4[Fe-:19]1[S+3:18]1[Fe-:20]2[S+:24]3[Fe-:22]41.[H:96][#8:53][C@:52]([H:95])([C:51]([H:92])([H:93])[H:94])[C:54]([H:97])([H:98])[H:99].[H:126][#7:72]([H:127])-[#6:71](=[O:73])[C:70]([H:124])([H:125])[C:69]([H:121])([H:122])[H:123].[H:100][C:55]([H:101])([H:102])[C:56]([H:103])([H:104])[C:57]([H:105])([H:106])[#6:58](-[#8-:59])=[O:60].[H:117][#7:67]([H:118])-[#6:66](-[#7:65]([H:116])[C:64]([H:114])([H:115])[C:63]([H:112])([H:113])[C:62]([H:110])([H:111])[C:61]([H:107])([H:108])[H:109])=[#7+:68]([H:119])[H:120].[H:128][#7:74]=[#6:27]1[#7:30]=,:[#6:31]([#7:28]([H:75])[#6:26](=,:[#6:25]1[C:50]([H:90])([H:91])[#7:39]1[#6+:33]([H:76])[#16:34][#6:37](=,:[#6:35]1[C:36]([H:83])([H:84])[H:85])[C:38]([H:86])([H:87])[C:40]([H:88])([H:89])[#8:41][P:48]([#8-:49])(=[O:42])[#8:43][P:46]([#8-:44])([#8-:45])=[O:47])-[#7:29]([H:78])[H:79])[C:32]([H:80])([H:81])[H:82]>>[Mg++:77].[S+:1]12[Fe-:3]3[S+:2]4[Fe-:4]1[S+:8]1[Fe-:5]2[S+:7]3[Fe-:6]41.[S+:9]12[Fe-:11]3[S+:10]4[Fe-:12]1[S+:16]1[Fe-:13]2[S+:15]3[Fe-:14]41.[S+:17]12[Fe-:21]3[S+:23]4[Fe-:19]1[S+3:18]1[Fe-:20]2[S+:24]3[Fe-:22]41.[H:96][#8:53][C@:52]([H:95])([C:51]([H:92])([H:93])[H:94])[C:54]([H:97])([H:98])[H:99].[H:126][#7:72]([H:127])-[#6:71](=[O:73])[C:70]([H:124])([H:125])[C:69]([H:121])([H:122])[H:123].[H:100][C:55]([H:101])([H:102])[C:56]([H:103])([H:104])[C:57]([H:105])([H:106])[#6:58](-[#8-:59])=[O:60].[H:117][#7:67]([H:118])-[#6:66](-[#7:65]([H:116])[C:64]([H:114])([H:115])[C:63]([H:112])([H:113])[C:62]([H:110])([H:111])[C:61]([H:107])([H:108])[H:109])=[#7+:68]([H:119])[H:120].[H:128][#7:74]=[#6:27]1[#7:30]=,:[#6:31]([#7:28]([H:75])[#6:26](=,:[#6:25]1[C:50]([H:90])([H:91])[#7+:39]1=,:[#6:33]([H:76])[#16:34][#6:37](=,:[#6:35]1[C:36]([H:83])([H:84])[H:85])[C:38]([H:86])([H:87])[C:40]([H:88])([H:89])[#8:41][P:48]([#8-:49])(=[O:42])[#8:43][P:46]([#8-:44])([#8-:45])=[O:47])-[#7:29]([H:78])[H:79])[C:32]([H:80])([H:81])[H:82]'
            ]:
        print(rxn_smi)
        print(get_changed_bonds(rxn_smi))

    # Process files
    # process_file('Ex23/a_ns_p_explicit_h.txt')
    # process_file('Ex23/a_ns_p_explicit.txt')
    # process_file('Ex23/na_ns_p.txt')
    # process_file('Ex23/na_ns_p_explicit_h.txt')

    # Ex25
    process_file('Ex25/a_explicit_h_rc_all.txt')

    # process_file('../data/valid.txt')
    # process_file('../data/test.txt')
    # process_file('../data/test_human.txt')

