import rdkit
import rdkit.Chem as Chem
import numpy as np

files = []

# files.append(["Ex23/a_ns_p_explicit_h_withname.txt", "Ex23/a_ns_p_explicit_h.txt"])
# files.append(["Ex23/a_ns_p_explicit_withname.txt", "Ex23/a_ns_p_explicit.txt"])
# files.append(["Ex23/na_ns_np_explicit_h_withname.txt", "Ex23/na_ns_np_explicit_h.txt"])
# files.append(["Ex23/na_ns_np_withname.txt", "Ex23/na_ns_np.txt"])
# files.append(["Ex23/na_ns_p_explicit_h_withname.txt", "Ex23/na_ns_p_explicit_h.txt"])
# files.append(["Ex23/na_ns_p_withname.txt", "Ex23/na_ns_p.txt"])

# Ex25
files.append(["Ex25/a_explicit_h_rc_withname.txt", "Ex25/a_explicit_h_rc.txt"])
files.append(["Ex25/na_explicit_h_rc_withname.txt", "Ex25/na_explicit_h_rc.txt"])

# TMPDIFF
# files.append(["gathered_data_implicit_withname.txt", "Split_Reaction/new_smiles.txt.proc"])
# files.append(["Split_Reaction/new_smiles_implicit_withname.txt.proc", "Split_Reaction/new_smiles_implicit.txt.proc"])

for f_pair in files:
    print(f_pair)
    with open(f_pair[0]) as f:
        content0 = f.readlines()

    with open(f_pair[1]) as f:
        content1 = f.readlines()

    content_array = [content0, content1]

    c = 0
    reacs = []
    for line in content0:
        tmp = line.split(">>")[0].split("  ")
        print(tmp)
        
        rname = tmp[0]
        rmols = tmp[1].split(".")
        pmols = line.split(">>")[1].split(" ")[0].split(".")

        reacs.append([rmols, pmols])

        c += 1

    for i in reversed(range(len(reacs))):
        skip = False
        for rmol in reacs[i][0]:
            tmp = Chem.MolFromSmiles(rmol, sanitize = False)
            try:
                tmp.UpdatePropertyCache()
                Chem.GetSymmSSSR(tmp)
            except:
                print(i+1)
                skip = True
            
        if skip == True:
            del content0[i]
            del content1[i]
        else:
            for pmol in reacs[i][1]:
                tmp = Chem.MolFromSmiles(pmol, sanitize = False)
                try:
                    tmp.UpdatePropertyCache()
                    Chem.GetSymmSSSR(tmp)
                except:
                    print(i+1)
                    del content0[i]
                    del content1[i]

    fout0 = open(f_pair[0], "w+")
    fout1 = open(f_pair[1], "w+")

    for line in content0:
        fout0.write(line)

    for line in content1:
        fout1.write(line)
        
    fout0.close()
    fout1.close()