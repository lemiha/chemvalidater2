import glob

# paths = [
#     "Ex23/a_ns_p_explicit_h_withname.txt",
#     "Ex23/a_ns_p_explicit_h.txt",
#     "Ex23/a_ns_p_explicit_withname.txt",
#     "Ex23/a_ns_p_explicit.txt"
# ]

paths = [
    "Ex25/a_explicit_h_rc_all.txt"
]

for fname in paths:
    with open(fname) as f:
        content = f.readlines()

    for i in range(len(content)):
        content[i] = content[i].replace("-0.0","")
        content[i] = content[i].replace("-1.0","")
        content[i] = content[i].replace("-1.5","")
        content[i] = content[i].replace("-2.0","")
        content[i] = content[i].replace("-3.0","")
        content[i] = content[i].replace("-4.0","")

    fout = open(fname, "w+")
    for line in content:
        fout.write(line)
    fout.close()

    # with open(files[0]) as f:
    #     without_content = f.readlines()

    # with_content = [x.strip() for x in with_content]
    # without_content = [x.strip() for x in without_content]

    # delete_indices = []
    # for i in reversed(range(len(with_content))):
    #     if ".[1]" in with_content[i] or ".[2]" in with_content[i]:
    #         del with_content[i]
    #         del without_content[i]

    # f_with_out = open(files[1], "w+")
    # for line in with_content:
    #     f_with_out.write(line + "\n")
    # f_with_out.close()

    # f_without_out = open(files[0], "w+")
    # for line in without_content:
    #     f_without_out.write(line + "\n")
    # f_without_out.close()




