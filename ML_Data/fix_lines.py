import glob

files = glob.glob("Ex23/*.tx*")

for fname in files:
    with open(fname) as f:
        smiles_content = f.readlines()
        
    for i in reversed(range(len(smiles_content))):
        if smiles_content[i] == "\n" :
            del smiles_content[i]

    fout = open(fname, "w+")
    for line in smiles_content:
        fout.write(line)
        
    fout.close()