import numpy as np
import subprocess
import shutil
import os
from functools import reduce
from validate_methods import Validation
import glob

# files_to_remove = glob.glob("ML_Data/*")
# for fr in files_to_remove:
#     os.remove(fr)

# # allh_all.txt.proc
# shutil.copyfile('gathered_data_explicit_withname.txt', 'ML_Data/allh_all.txt.proc')

# # allh_rc_all.txt.proc
# subprocess.run(['python3', 'keep_only.py', 'gathered_data_explicit_withname.txt', 'ML_Data/allh_rc_all.txt.proc'])

# # one_h_all.txt.proc
# shutil.copyfile('gathered_data_explicit_h_withname.txt', 'ML_Data/one_h_all.txt.proc')

# one_h_rc_all.txt.proc
# subprocess.run(['python3', 'keep_only.py', 'Ex23/na_ns_np.txt', 'Ex23/na_ns_np_rc.txt'])
# subprocess.run(['python3', 'keep_only.py', 'Ex23/na_ns_np_explicit_h.txt', 'Ex23/na_ns_np_explicit_h_rc.txt'])
# subprocess.run(['python3', 'keep_only.py', 'Ex23/a_ns_p_explicit.txt.proc', 'Ex23/a_ns_p_explicit_rc.txt.proc'])
# subprocess.run(['python3', 'keep_only.py', 'Ex23/a_ns_p_explicit_h.txt.proc', 'Ex23/a_ns_p_explicit_h_rc.txt.proc'])
# subprocess.run(['python3', 'keep_only.py', 'Ex23/na_ns_p_explicit_h.txt.proc', 'Ex23/na_ns_p_explicit_h_rc.txt.proc'])
# subprocess.run(['python3', 'keep_only.py', 'Ex23/na_ns_p.txt.proc', 'Ex23/na_ns_p_rc.txt.proc'])


# all_files = [
#             "Ex23/a_ns_np_explicit_h_rc_all.txt.proc",
#             "Ex23/a_ns_np_explicit_h_all.txt.proc",
#             "Ex23/a_ns_np_explicit_rc_all.txt.proc",
#             "Ex23/a_ns_np_explicit_all.txt.proc",
#             "Ex23/a_ns_p_explicit_h_rc_all.txt.proc",
#             "Ex23/a_ns_p_explicit_h_all.txt.proc",
#             "Ex23/a_ns_p_explicit_rc_all.txt.proc",
#             "Ex23/a_ns_p_explicit_all.txt.proc",
#             "Ex23/na_ns_np_explicit_h_rc_all.txt.proc",
#             "Ex23/na_ns_np_explicit_h_all.txt.proc",
#             "Ex23/na_ns_np_rc_all.txt.proc",
#             "Ex23/na_ns_np_all.txt.proc",
#             "Ex23/na_ns_p_explicit_h_rc_all.txt.proc",
#             "Ex23/na_ns_p_explicit_h_all.txt.proc",
#             "Ex23/na_ns_p_rc_all.txt.proc",
#             "Ex23/na_ns_p_all.txt.proc"
# ]

# Ex25
all_files = [
        "Ex25/a_explicit_h_rc_all.txt.proc",
        "Ex25/na_explicit_h_rc_all.txt.proc"
]


val = Validation()

tmp_all = []
for fname in all_files:
    with open(fname) as f:
        content = f.readlines()

    content = [x.strip() for x in content]

    tmp_pair = []
    for smiles in content:
        tmp = smiles.split("  ")
        tmp_name = tmp[0]
        tmp_smiles = tmp[1]

        tmp_pair.append([tmp_name, tmp_smiles])

    tmp_all.append(np.array(tmp_pair))

print(len(tmp_pair))

tmp_all = val.removeFailedMols(tmp_all)

entries = []
for tmp in tmp_all:
    entries.append(tmp[:,0])

entries_intersected = reduce(np.intersect1d, entries)

for i in range(len(tmp_all)):
    indices = np.where(np.isin(tmp_all[i][:,0], entries_intersected))
    tmp_all[i] = tmp_all[i][indices]

    # Sort
    sort_indices = np.argsort(tmp_all[i][:,0])
    tmp_all[i] = tmp_all[i][sort_indices]

    rng = np.random.default_rng(11111)
    rng.shuffle(tmp_all[i])


for i in range(len(all_files)):
    f_out_wo_name = all_files[i]
    # f_out_w_name = all_files[i].split(".txt.proc")[0] + "_withname" + ".txt.proc"

    f_out_wo = open(f_out_wo_name, "w+")
    # f_out_w = open(f_out_w_name, "w+")

    for smi in tmp_all[i]:
        f_out_wo.write(smi[0] + "  " + smi[1] + "\n")
        # f_out_w.write(smi[0] + "  " + smi[1] + "\n")

    f_out_wo.close()
    # f_out_w.close()

# Split into train, validation and test set
train_size = np.arange(start=0, stop=1850, step=1)
val_size = np.arange(start=1850, stop=len(tmp_all[0]), step=1)
test_size = np.array([])#np.arange(start=2400+(len(tmp_all[0])-200), stop=len(tmp_all[0]), step=1)

filter_sizes = [train_size, val_size, test_size]

for i in range(len(tmp_all)):
    for j in range(len(filter_sizes)):
        if j == 0:            
            f_out_wo_name = all_files[i].replace("_all", "_train")
            # f_out_w_name = all_files[i].split(".txt.proc")[0] + "_withname" + ".txt.proc"
            # # f_out_w_name = f_out_w_name.replace("_all", "_train")
        elif j == 1:
            f_out_wo_name = all_files[i].replace("_all", "_val")
            # f_out_w_name = all_files[i].split(".txt.proc")[0] + "_withname" + ".txt.proc"
            # # f_out_w_name = f_out_w_name.replace("_all", "_val")
        else:
            f_out_wo_name = all_files[i].replace("_all", "_test")
            # f_out_w_name = all_files[i].split(".txt.proc")[0] + "_withname" + ".txt.proc"
            # # f_out_w_name = f_out_w_name.replace("_all", "_test")

        f_out_wo = open(f_out_wo_name, "w+")
        # f_out_w = open(f_out_w_name, "w+")

        if len(filter_sizes[j]) == 0:
            f_out_wo.close()
            # f_out_w.close()
            continue
        else:
            for smi in tmp_all[i][filter_sizes[j]]:
                f_out_wo.write(smi[0] + "  " + smi[1] + "\n")
                # f_out_w.write(smi[0] + "  " + smi[1] + "\n")

        f_out_wo.close()
        # f_out_w.close()

        





# allh_train.txt.proc
# allh_val.txt.proc
# allh_test.txt.proc

# allh_rc_train.txt.proc
# allh_rc_val.txt.proc
# allh_rc_test.txt.proc

# one_h_train.txt.proc
# one_h_test.txt.proc
# one_h_val.txt.proc

# one_h_rc_train.txt.proc
# one_h_rc_test.txt.proc
# one_h_rc_val.txt.proc
