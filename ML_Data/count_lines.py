import glob

files = glob.glob("Ex23/*")

for fname in files:
    with open(fname) as f:
        content = f.readlines()

    if len(content) == 0:
        print(str(fname) + "   " + str(len(content)))