import glob

# files = glob.glob("Ex23/*.tx*")

# Ex25
files = glob.glob("Ex25/*.tx*")

to_delete_reacname = []
for fname in files:
    with open(fname) as f:
        smiles_content = f.readlines()
        
    smiles_content = [x.strip() for x in smiles_content]

    for i in reversed(range(len(smiles_content))):
        tmp = smiles_content[i].split("  ")
        tmp_name = tmp[0]
        tmp_smiles = tmp[1].split(" ")

        try:
            edits = tmp_smiles[1].split(";")
        except:
            to_delete_reacname.append(tmp_name)


for fname in files:
    with open(fname) as f:
        smiles_content = f.readlines()

    smiles_content = [x.strip() for x in smiles_content]
        
    for i in reversed(range(len(smiles_content))):
        tmp = smiles_content[i].split("  ")
        tmp_name = tmp[0]
        tmp_smiles = tmp[1].split(" ")

        if tmp_name in to_delete_reacname:
            del smiles_content[i]

    fout = open(fname, "w+")
    for line in smiles_content:
        fout.write(line + "\n")
    fout.close()