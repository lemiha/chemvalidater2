import glob

# files = glob.glob("Ex20/*.proc")
# print(files)
files_paths = []
# files_paths.append(["Ex23/a_ns_p_explicit_h.txt", "Ex23/a_ns_p_explicit_h_withname.txt"])
# files_paths.append(["Ex23/a_ns_p_explicit.txt", "Ex23/a_ns_p_explicit_withname.txt"])
# files_paths.append(["Ex23/na_ns_np_explicit_h.txt", "Ex23/na_ns_np_explicit_h_withname.txt"])
# files_paths.append(["Ex23/na_ns_np.txt", "Ex23/na_ns_np_withname.txt"])
# files_paths.append(["Ex23/na_ns_p_explicit_h.txt", "Ex23/na_ns_p_explicit_h_withname.txt"])
# files_paths.append(["Ex23/na_ns_p.txt", "Ex23/na_ns_p_withname.txt"])

# Ex25
files_paths = [
    # "Ex25/a_explicit_h_rc_all.txt",
    # "Ex25/a_explicit_h_rc_all.txt.proc",
    # "Ex25/na_explicit_h_rc_all.txt.proc"
    "../Statistics/Nr_of_bonds_data/gathered_data_explicit_withname.txt"

]


for files in files_paths:
    with open(files) as f:
        with_content = f.readlines()
    # print(with_content)

    with_content = [x.strip() for x in with_content]

    delete_indices = []
    for i in reversed(range(len(with_content))):
        if ".[1]" in with_content[i]:
            del with_content[i]
    

    f_with_out = open(files, "w+")
    for line in with_content:
        f_with_out.write(line + "\n")
    f_with_out.close()



