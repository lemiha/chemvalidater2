import sys
import glob
import os
from shutil import copyfile
import subprocess

counter = 0

from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(2)

args = sys.argv

# Gather data
origin = "Data_Extraction/Fixed_Data/*.mrv"
src_files = []
for file in glob.glob(origin):
    src_files.append(file)

src_files.sort()

# If implicit
if args[1] == "implicit":

    src_files.sort()

    dst_files = []

    for file in src_files:
        tmp = file.split("/")
        dst = "implicit_data/reactants/" + tmp[2] 

        copyfile(file, dst)

# If explicit
elif args[1] == "explicit":
    all_dst = "explicit_data/reactants/.mrv"
    
    # Convert all files
    command = "molconvert" + " " + "mrv:H" + " " + str(origin) + " " + "-o" + " " + str(all_dst) + " " + "-m"
    subprocess.run(command, shell=True)

    # Rename files, such that they can be sorted
    out_dst = "explicit_data/reactants/*.mrv"
    out_files = []
    for file in glob.glob(out_dst):
        tmp = file.split(".mrv")[0].split("/")
        print(tmp)
        if len(tmp[2]) == 1:
            tmp[2] = "000" + tmp[2]
        elif len(tmp[2]) == 2:
            tmp[2] = "00" + tmp[2]
        elif len(tmp[2]) == 3:
            tmp[2] = "0" + tmp[2]

        newf_name = "/".join(tmp) + ".mrv"

        os.rename(file, newf_name)
        out_files.append(newf_name)
    
    out_files.sort()

    # Rename files correctly
    for i in range(len(src_files)):
        tmp = src_files[i].split("/")
        f_dst = "explicit_data/reactants/" + tmp[2]
        
        os.rename(out_files[i], f_dst)


# If either explicit or implicit
else:
    print("Not correct input")


