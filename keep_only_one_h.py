import numpy as np
from manipulations import Reaction
import sys
import glob

args = sys.argv
files = []
for file in glob.glob(args[1] + "/*.mrv"):
    files.append(file)

# Overlook files with single electron pushing
for i in reversed(range(len(files))):
    with open(files[i]) as f:
        content = f.readlines()
    for line in content:
        # Half arrows is removed
        if "headFlags=\"2\"" in line:
            del files[i]
            break

files.sort()

# files = ["explicit_data/reactants/Step_macie.0002.02.01.mrv"]
# for fname in files:
#     r = Reaction()
#     outf = "explicit_data_oneh/reactants/" + fname.split("/")[2]
#     r.keepOneHydrogen(fname, outf)

# Remove the hydrogne
names_failed = []
for fname in files:
    r = Reaction()
    try:
        outf = "explicit_data_oneh/reactants/" + fname.split("/")[2]
        r.keepOneHydrogen(fname, outf)
    except:
        print(fname)
        names_failed.append(fname)

# Print error files
f_exclude = open("excluded_files.txt","w+")

for fname in names_failed:
    f_exclude.write(str(fname)  + "\n")

f_exclude.close()