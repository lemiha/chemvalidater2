import numpy as np
import glob
import sys
from compare_methods import Compare

args = sys.argv

com = Compare()

rarray, sarray = com.loadData(args, compare_reactants = False)

labels = com.getLabels(rarray)
correct_ind = com.correctLabels(labels)

com.findChargesMismatch(sarray, rarray)
com.examineDiff(sarray, rarray)
com.examineFails(sarray, rarray)
com.printStats(sarray, rarray)
com.printLatexFails(sarray, rarray)
# com.examineMatches(sarray, rarray)