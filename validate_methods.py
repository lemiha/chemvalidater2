import rdkit
import rdkit.Chem as Chem
import numpy as np

class Validation:
    def removeFailedMols(self, file_contents):
        for i in range(len(file_contents)):
            c = 0
            reacs = []
            for pair in file_contents[i]:
                tmp = pair[1].split(">>")[0]
                
                rmols = tmp.split(".")
                pmols = pair[1].split(">>")[1].split(" ")[0].split(".")

                reacs.append([rmols, pmols])

                c += 1

            delete_indices = []
            for j in reversed(range(len(reacs))):
                skip = False
                for rmol in reacs[j][0]:
                    tmp = Chem.MolFromSmiles(rmol, sanitize = False)
                    try:
                        tmp.UpdatePropertyCache()
                        Chem.GetSymmSSSR(tmp)
                    except:
                        print(j+1)
                        skip = True
                    
                if skip == True:
                    delete_indices.append(j)
                else:
                    for pmol in reacs[j][1]:
                        tmp = Chem.MolFromSmiles(pmol, sanitize = False)
                        try:
                            tmp.UpdatePropertyCache()
                            Chem.GetSymmSSSR(tmp)
                        except:
                            print(j+1)
                            delete_indices.append(j)

            file_contents[i] = np.delete(file_contents[i], delete_indices, axis=0)

        return file_contents



            