import numpy as np
import re
import sys


def multireplace(string, replacements, ignore_case=False):
    """
    Given a string and a replacement map, it returns the replaced string.
    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :param bool ignore_case: whether the match should be case insensitive
    :rtype: str
    """
    # If case insensitive, we need to normalize the old string so that later a replacement
    # can be found. For instance with {"HEY": "lol"} we should match and find a replacement for "hey",
    # "HEY", "hEy", etc.
    if ignore_case:
        def normalize_old(s):
            return s.lower()

        re_mode = re.IGNORECASE

    else:
        def normalize_old(s):
            return s

        re_mode = 0

    replacements = {normalize_old(key): val for key, val in replacements.items()}
    
    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    rep_sorted = sorted(replacements, key=len, reverse=True)
    rep_escaped = map(re.escape, rep_sorted)
    
    # Create a big OR regex that matches any of the substrings to replace
    pattern = re.compile("|".join(rep_escaped), re_mode)
    
    # For each match, look up the new string in the replacements, being the key the normalized old string
    return pattern.sub(lambda match: replacements[normalize_old(match.group(0))], string)

def updateIdentifiers(smiles):
    
    pattern = "\:(.*?)\]"
    matches = re.findall(pattern, smiles, re.DOTALL)
    indexes = np.unique(matches, return_index=True)[1]

    matches_or = [matches[index] for index in sorted(indexes)]
    matches_smiles = [":"+str(x)+"]" for x in matches_or]
    matches_edits = [str(x)+"-" for x in matches_or]

    replace_ids = np.arange(1,len(matches_smiles)+1)
    replace_ids_smiles = [":"+str(x)+"]" for x in replace_ids]
    replace_ids_edits = [str(x)+"-" for x in replace_ids]

    dict_reac = dict(zip(matches_smiles, replace_ids_smiles))
    dict_edits = dict(zip(matches_edits, replace_ids_edits))

    new_smi = multireplace(smiles, dict_reac)
    new_smi = multireplace(new_smi, dict_edits)

    return new_smi
    # tmp = re.sub(':.*?]', matches, smiles)
    # print(matches)
    
    # print(tmp)

args = sys.argv

data_f = args[1]

with open(data_f) as f:
    smiles_content = f.readlines()
    
smiles_content = [x.strip() for x in smiles_content]

f_out = open(args[2], "w+")

for smiles in smiles_content:
    tmp = smiles.split("  ")
    tmp_name = tmp[0]
    tmp_smiles = tmp[1].split(" ")

    reac = np.array(tmp_smiles[0].split(">>")[0].split("."))
    prod = np.array(tmp_smiles[0].split(">>")[1].split("."))
    edits = tmp_smiles[1].split(";")
    edits_nochange = [x.split("-") for x in edits]

    ids = []
    for e in edits_nochange:
        ids.append(":" + e[0] + "]")
        ids.append(":" + e[1] + "]")

    ids = np.unique(ids)

    # Reac indices
    reac_mols_indices = []
    for id in ids:
        for j in range(len(reac)):
            if id in reac[j]:
                reac_mols_indices.append(j)

    reac_mols_indices = np.unique(reac_mols_indices)

    # Prod indices
    prod_mols_indices = []
    for id in ids:
        for j in range(len(prod)):
            if id in prod[j]:
                prod_mols_indices.append(j)

    prod_mols_indices = np.unique(prod_mols_indices)

    # Gather
    new_reac = ".".join(reac[reac_mols_indices])


    new_prod = ".".join(prod[prod_mols_indices])

    new_smiles = new_reac + ">>" + new_prod + " " + tmp_smiles[1]
    new_smiles = updateIdentifiers(new_smiles)
    f_out.write(tmp_name + "  " + new_smiles + "\n")
    
    # break

f_out.close()