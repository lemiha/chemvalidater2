import subprocess
import glob, os

# folder1 = 'Steps'
# folder2 = 'Steps_id'

# # Make the reactions for both the mrv-files with and without atom-atom mapping
paths = []
paths.append('implicit_data/reactants/*')
paths.append('explicit_data/reactants/*')

paths.append('implicit_data/products/*')
paths.append('implicit_data/reactants_id/*')
paths.append('implicit_data/products_id/*')
paths.append('explicit_data/reactants_id/*')
paths.append('explicit_data/products_id/*')
paths.append('explicit_data/products/*')

# paths.append('implicit_data/smiles_reactants_id/*')
# paths.append('implicit_data/smiles_products_id/*')
# paths.append('explicit_data/smiles_reactants_id/*')
# paths.append('explicit_data/smiles_products_id/*')
# paths.append('implicit_data/smiles_reactants/*')
# paths.append('implicit_data/smiles_products/*')
# paths.append('explicit_data/smiles_reactants/*')
# paths.append('explicit_data/smiles_products/*')

for path in paths:
    for f in glob.glob(path):
        os.remove(f)