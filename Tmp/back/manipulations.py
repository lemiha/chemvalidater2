import copy
import numpy as np
b_types = ["N", "H", "W"]

class Reaction:
    def __init__(self):
        self.updated_ids = []
        self.list_of_atoms = []
        self.list_of_atoms_or = []
        self.list_of_bonds = []
        self.list_of_bonds_or = []
        self.single_e = []
        self.double_e = []

    def collectData(self, fname):
        skipM2 = False

        with open(fname) as f:
            content = f.readlines()

        content = [x.strip() for x in content]
        
        i_old = None
        for i in range(len(content)):
            if "<atom id=" in content[i]:
                tmp_atomid = content[i].split("<atom id=\"")[1].split("\" el")[0].split('a')[1]
                tmp_atomid = int(tmp_atomid)
                tmp_atomname = content[i].split("elementType=\"")[1]

                if tmp_atomname[1] == "\"":
                    tmp_atomname = tmp_atomname[0]
                else:
                    tmp_atomname = tmp_atomname[0:2]

                if "formalCharge" in content[i]:
                    tmp_oldfcharge = content[i].split("formalCharge=\"")[1]

                    if tmp_oldfcharge[0] == "-":
                        tmp_oldfcharge = int(tmp_oldfcharge[0:2])
                    else:
                        tmp_oldfcharge = int(tmp_oldfcharge[0])
                else:
                    tmp_oldfcharge = 0

                if "lonePair" in content[i]:
                    tmp_oldlonep = content[i].split("lonePair=\"")[1]

                    if tmp_oldlonep[0] == "-":
                        tmp_oldlonep = int(tmp_oldlonep[0:2])
                    else:
                        tmp_oldlonep = int(tmp_oldlonep[0])
                else:
                    tmp_oldlonep = 0

                self.list_of_atoms.append([tmp_atomid, tmp_atomname, tmp_oldfcharge, tmp_oldlonep])

            if "molID=\"m2\"" in content[i] or "molecule id=\"sg1\"" in content[i] or "role=\"SuperatomSgroup\"" in content[i]:
                skipM2 = True

            if "atomRefs2" in content[i] and skipM2 == False:
                if "convention" in content[i]:
                    tmp_bond = content[i].split("\" convention=")[0].split("atomRefs2=\"")[1].split(" ")
                    tmp_order = "-5"
                else:
                    tmp_bond = content[i].split("fs2=\"")[1].split("\" o")[0].split(" ")
                    tmp_order = content[i].split("order=\"")[1][0]

                if "<bondStereo>" in content[i+1]:
                    bond_type = content[i+1].split("<bondStereo>")[1].split("</bond")[0]
                    if bond_type == "H":
                        bond_type = 1
                    else: # if "W"
                        bond_type = 2
                else:
                    bond_type = 0
                
                # print(tmp_bond[0].split('a')[1])
                # print(tmp_bond[1].split('a')[1])
                # print(tmp_order)
                # print(bond_type)
                tmp = [int(tmp_bond[0].split('a')[1]), int(tmp_bond[1].split('a')[1]), int(tmp_order), bond_type]
                self.list_of_bonds.append(tmp)

            if ("MAtomSetPoint atomRefs" in content[i] or "MEFlowBasePoint atomRef" in content[i]) and i_old != i-1:
                tmp1 = content[i]
                tmp2 = content[i+1]

                if ("headFlags=\"2\"" in content[i-1]) or ("headFlags=\"2\"" in content[i-2]):
                    self.single_e.append([tmp1, tmp2])
                else:
                    self.double_e.append([tmp1, tmp2])
                
                i_old = i

    # Get the list of reactions
    def updateReactions(self):
        single_eupdated = []
        double_eupdated = []
        
        for reac in self.single_e:
            # print("reac   " + str(reac))
            from_r = reac[0].replace("m1.a", "")
            to_r = reac[1].replace("m1.a", "")

            if "mRefs=" in from_r:
                from_r = from_r.split("mRefs=\"")[1].split("\"/>")[0].split(" ")
            else:
                from_r = from_r.split("mRef=\"")[1].split("\"/>")[0].split(" ")

            if "mRefs=" in to_r:
                to_r = to_r.split("mRefs=\"")[1].split("\"/>")[0].split(" ")
            else:
                to_r = to_r.split("mRef=\"")[1].split("\"/>")[0].split(" ")
            
            from_r = list(map(int, from_r))
            to_r = list(map(int, to_r))

            single_eupdated.append([from_r, to_r])

        for reac in self.double_e:
            # print("reac   " + str(reac))
            from_r = reac[0].replace("m1.a", "")
            to_r = reac[1].replace("m1.a", "")

            if "mRefs=" in from_r:
                from_r = from_r.split("mRefs=\"")[1].split("\"/>")[0].split(" ")
            else:
                from_r = from_r.split("mRef=\"")[1].split("\"/>")[0].split(" ")

            if "mRefs=" in to_r:
                to_r = to_r.split("mRefs=\"")[1].split("\"/>")[0].split(" ")
            else:
                to_r = to_r.split("mRef=\"")[1].split("\"/>")[0].split(" ")
            
            from_r = list(map(int, from_r))
            to_r = list(map(int, to_r))

            double_eupdated.append([from_r, to_r])
            
        self.single_e = single_eupdated
        self.double_e = double_eupdated

    def getNeighboringBonds(self, atom_id):
        f_a0 = np.where(self.list_of_bonds[:,0] == atom_id)
        s_a0 = np.where(self.list_of_bonds[:,1] == atom_id)

        bonds = np.concatenate([f_a0[0], s_a0[0]])
        bond_indices = []
        for index in bonds:
            if self.list_of_bonds[index][2] != -5:
                bond_indices.append(index)
        
        bonds_indices = self.list_of_bonds[bond_indices]

        return bonds_indices

    def getNeighboringAtoms(self, atom_id):
        neighbors = self.getNeighboringBonds(atom_id)
        neighbors = np.concatenate([neighbors[:,0], neighbors[:,1]])
        neighbors = np.unique(neighbors)
        neighbors = np.delete(neighbors, np.argwhere(neighbors == atom_id))

        return neighbors     

    def getReactingAtoms(self):
        reacting_atoms = np.array(self.double_e).flatten()
        reacting_atoms = reacting_atoms[reacting_atoms != np.array(None)] # Remove None's
        reacting_atoms = np.unique(reacting_atoms)
        
        return reacting_atoms


    def isAllNeighCoor(self, atom):
        bond_types = self.getNeighboringBonds(atom)
        
        coor_is_in = False
        if len(bond_types) > 1:
            if bond_types[:,2][0] == -5 and len(set(bond_types[:,2])) == 1:
                coor_is_in = True

        return coor_is_in

    # If a bond is removed
    def removeBond(self, before, after):
        f_b0 = np.where(self.list_of_bonds[:,0] == before[0])
        s_b0 = np.where(self.list_of_bonds[:,1] == before[0])
        f_b1 = np.where(self.list_of_bonds[:,0] == before[1])
        s_b1 = np.where(self.list_of_bonds[:,1] == before[1])
        
        intersec1 = np.intersect1d(f_b0, s_b1)
        intersec2 = np.intersect1d(s_b0, f_b1)
        
        try:
            if len(intersec1) != 0:
                index = intersec1[0]
            elif len(intersec2) != 0:
                index = intersec2[0]
        except ValueError:
            print("Error")

        skip_charge = False
        if self.list_of_bonds[index][2] == 1:
            self.list_of_bonds = np.delete(self.list_of_bonds, index, 0)
        elif self.list_of_bonds[index][2] == -5:
            skip_charge = True
            self.list_of_bonds = np.delete(self.list_of_bonds, index, 0)
        else:
            self.list_of_bonds[index][2] -= 1

        return skip_charge

    def getScalar(self, atom_id):
        # element = self.list_of_atoms[atom_id-1][1]

        # if element in ["C", "V", "H", "Fe"]:
        #     return -1
        # else:
        #     return 1

        return 1

    def getBonds(self, atom_id, original):
        sum_bonds = 0
        if original == False:
            fm = np.where(self.list_of_bonds[:,0] == atom_id)
            sm = np.where(self.list_of_bonds[:,1] == atom_id)

            for f in fm[0]:
                tmp_nr = self.list_of_bonds[f][2]
                if tmp_nr < -1:
                    continue
                else:
                    sum_bonds += tmp_nr

            for s in sm[0]:
                tmp_nr = self.list_of_bonds[s][2]
                if tmp_nr < -1:
                    continue
                else:
                    sum_bonds += tmp_nr
        else:
            fm = np.where(self.list_of_bonds_or[:,0] == atom_id)
            sm = np.where(self.list_of_bonds_or[:,1] == atom_id)

            for f in fm[0]:
                sum_bonds += self.list_of_bonds_or[f][2]

            for s in sm[0]:
                sum_bonds += self.list_of_bonds_or[s][2]
        
        return sum_bonds

    def isDifferentMolecule(self, before, after):
        tmp = before + after
        combined = np.unique(tmp)

        for i in range(len(combined)):
            combined[i] = combined[i] -1

        connected_graph = np.array(self.g.connectedComponents())
        allin = False
        for mol in connected_graph:
            if all(np.isin(combined, mol)) == True:
                print(combined)
                print(mol)
                allin = True
                break

        return allin



    def updateCharge(self, old_reac, skip_c, before, after):        
        
        before_old = old_reac[0]
        after_old = old_reac[1]

        # Special case:
        if (len(before_old) == 2 and len(after_old) == 1) and (len(before) == 2 and len(after) == 2):
            if after_old[0] in after:
                if before[0] not in after:
                    self.list_of_atoms[before[0]-1][2] -= 1
                else:
                    self.list_of_atoms[before[1]-1][2] -= 1

                if after[0] not in before:
                    self.list_of_atoms[after[0]-1][2] += 1
                else:
                    self.list_of_atoms[after[1]-1][2] += 1

                return
                

        
        # If changed
        if len(before) == 2 and len(after) == 2:
            # if self.isDifferentMolecule(before, after):            
            if skip_c == False:
                if before[0] not in after:
                    self.list_of_atoms[before[0]-1][2] += 1
                else:
                    self.list_of_atoms[before[1]-1][2] += 1
            else:
                if before[0] in after:
                    self.list_of_atoms[before[0]-1][2] -= 1
                else:
                    self.list_of_atoms[before[1]-1][2] -= 1

            if after[0] not in before:
                self.list_of_atoms[after[0]-1][2] -= 1
            else:
                self.list_of_atoms[after[1]-1][2] -= 1


            # else:
            #     if before[0] not in after:
            #         self.list_of_atoms[before[0]-1][2] -= 1
            #     else:
            #         self.list_of_atoms[before[1]-1][2] -= 1

            #     if after[0] not in before:
            #         self.list_of_atoms[after[0]-1][2] += 1
            #     else:
            #         self.list_of_atoms[after[1]-1][2] += 1

        # If added
        if len(before) == 1 and len(after) == 2:
            # if after[0] == before[0]:
            #     self.list_of_atoms[after[0]-1][2] += 1
            # else:
            #     self.list_of_atoms[after[1]-1][2] += 1
            if skip_c == False:
                self.list_of_atoms[before[0]-1][2] += 1

                if after[0] not in before:
                    self.list_of_atoms[after[0]-1][2] -= 1
                else:
                    self.list_of_atoms[after[1]-1][2] -= 1

        # If removed
        if len(before) == 2 and len(after) == 1:
            if skip_c == False:
                if before[0] not in after:
                    self.list_of_atoms[before[0]-1][2] += 1
                else:
                    self.list_of_atoms[before[1]-1][2] += 1
                
                self.list_of_atoms[after[0]-1][2] -= 1

        # If one electron is shifted
        if len(before) == 1 and len(after) == 1:
            # Get bond index
            f_b0 = np.where(self.list_of_bonds[:,0] == before[0])
            s_b0 = np.where(self.list_of_bonds[:,1] == before[0])
            f_b1 = np.where(self.list_of_bonds[:,0] == after[0])
            s_b1 = np.where(self.list_of_bonds[:,1] == after[0])

            intersec1 = np.intersect1d(f_b0, s_b1)
            intersec2 = np.intersect1d(s_b0, f_b1)

            index = None
            if len(intersec1) != 0:
                index = intersec1[0]
            elif len(intersec2) != 0:
                index = intersec2[0]

            if index == None:
                self.list_of_atoms[before[0]-1][2] += 1
                self.list_of_atoms[after[0]-1][2] -= 1
            else:
                if self.list_of_bonds[index][2] == -5:
                    self.list_of_atoms[before[0]-1][2] += 2
                    self.list_of_atoms[after[0]-1][2] -= 2
                else:
                    self.list_of_atoms[before[0]-1][2] += 1
                    self.list_of_atoms[after[0]-1][2] -= 1


    #  If a bond is changed
    def changeBond(self, before, after):
        # Get index of before
        f_b0 = np.where(self.list_of_bonds[:,0] == before[0])
        s_b0 = np.where(self.list_of_bonds[:,1] == before[0])
        f_b1 = np.where(self.list_of_bonds[:,0] == before[1])
        s_b1 = np.where(self.list_of_bonds[:,1] == before[1])

        intersec1_before = np.intersect1d(f_b0, s_b1)
        intersec2_before = np.intersect1d(s_b0, f_b1)

        try:
            if len(intersec1_before) != 0:
                index_before = intersec1_before[0]
            elif len(intersec2_before) != 0:
                index_before = intersec2_before[0]
        except ValueError:
            print("Error")

        # Get index fo after
        f_a0 = np.where(self.list_of_bonds[:,0] == after[0])
        s_a0 = np.where(self.list_of_bonds[:,1] == after[0])
        f_a1 = np.where(self.list_of_bonds[:,0] == after[1])
        s_a1 = np.where(self.list_of_bonds[:,1] == after[1])

        intersec1_after = np.intersect1d(f_a0, s_a1)
        intersec2_after = np.intersect1d(s_a0, f_a1)

        if len(intersec1_after) != 0:
            index_after = intersec1_after[0]
        elif len(intersec2_after) != 0:
            index_after = intersec2_after[0]
        else:
            index_after = -1

        if after[0] not in before:
            gaining_atom = after[0]
        else:
            gaining_atom = after[1]

        coor_bond = self.isAllNeighCoor(gaining_atom)

        # Update the bonds
        if self.list_of_bonds[index_before][2] == 1:
            self.list_of_bonds = np.delete(self.list_of_bonds, index_before, 0)
            if index_before < index_after:
                index_after -= 1
        else:
            self.list_of_bonds[index_before][2] -= 1

        skip_charge = False
        if index_after == -1:
            if coor_bond == False:
                tmp = np.array([after[0], after[1], 1, 0])
            else:
                skip_charge = True
                tmp = np.array([after[0], after[1], -5, 0])

            self.list_of_bonds = np.vstack((self.list_of_bonds, tmp))
        else:
            self.list_of_bonds[index_after][2] += 1 

        return skip_charge

    def addBond(self, before, after):
        f_a0 = np.where(self.list_of_bonds[:,0] == after[0])
        s_a0 = np.where(self.list_of_bonds[:,1] == after[0])

        f_a1 = np.where(self.list_of_bonds[:,0] == after[1])
        s_a1 = np.where(self.list_of_bonds[:,1] == after[1])

        intersec1 = np.intersect1d(f_a0, s_a1)
        intersec2 = np.intersect1d(s_a0, f_a1)

        if len(intersec1) != 0:
            index_after = intersec1[0]
        elif len(intersec2) != 0:
            index_after = intersec2[0]
        else:
            index_after = -1
        
        if after[0] not in before:
            gaining_atom = after[0]
        else:
            gaining_atom = after[1]

        coor_bond = self.isAllNeighCoor(gaining_atom)

        skip_charge = False
        if index_after == -1:
            if coor_bond == False:
                tmp = np.array([after[0], after[1], 1, 0])
            else:
                skip_charge = True
                tmp = np.array([after[0], after[1], -5, 0])

            self.list_of_bonds = np.vstack((self.list_of_bonds, tmp))
        else:
            self.list_of_bonds[index_after][2] += 1
        
        return skip_charge

    def saveReaction(self, fname, update_atom_ids = False, outfile = None):

        if update_atom_ids == True:
            outf = outfile
        else: 
            tmp = fname.split("/")[2].split('mrv')[0]
            if fname.split("/")[1] == "reactants":
                outf = fname.split("/")[0] + '/' + 'products/' + tmp + '_ur.mrv'
            else:
                outf = fname.split("/")[0] + '/' + 'products_id/' + tmp + '_ur.mrv'

        with open(fname) as f:
            content = f.readlines()

        content = [x.strip() for x in content] 

        # The bonds are updated
        indices = []
        for i in range(len(content)):
            if content[i] == "<bondArray>":
                indices.append(i)

            if content[i] == "</bondArray>":
                indices.append(i)
                break

        start = "<bond atomRefs2=\""
        middle_1 = "\" order=\""
        middle_2 = "\" id=\""
        end = ["\"/>", "\">", "\">"]
        tmp2 = "</bond>"

        tmp_bonds = []
        for i in range(len(self.list_of_bonds)):
            a1 = self.list_of_bonds[i][0]
            a2 = self.list_of_bonds[i][1]
            order = self.list_of_bonds[i][2]
            b_id = "b" + str(i+1)
            bond_t = self.list_of_bonds[i][3]

            if order < 0:
                tmp_s = start + "a"+str(a1) + " a"+str(a2) + "\" convention=\"cxn:coord" + middle_2 + b_id + end[0]
            else:
                tmp_s = start + "a"+str(a1) + " a"+str(a2) + middle_1 + str(order) + middle_2 + b_id + end[bond_t]
            
            tmp_bonds.append(tmp_s)

            if bond_t == 1 or bond_t == 2:
                tmp1 = "<bondStereo>" + b_types[bond_t] + "</bondStereo>"
                tmp_bonds.append(tmp1)
                tmp_bonds.append(tmp2)

        # Print to file
        b_range = list(range(indices[0]+1)) + list(range(indices[1], len(content))) 
        
        
        with open(outf, 'w') as f:
            for i in b_range:
                if "<atom id=" in content[i]:
                    atom = content[i].split(" ")
                    tmp_id = int(atom[1].split("id=\"a")[1].split("\"")[0])

                    if update_atom_ids == True:
                        try:
                            new_id = self.updated_ids[tmp_id]
                        except:
                            continue

                        atom[1] = atom[1].replace("\"a"+str(tmp_id)+"\"", "\"a"+str(new_id)+"\"")
                    else:
                        new_id = tmp_id
    
                    # Formalcharge
                    tmp_oldfc = 0
                    index_fc = None
                    for j in range(len(atom)):
                        if "formalCharge" in atom[j]:
                            tmp_oldfc = int(atom[j].split("formalCharge=\"")[1].split("\"")[0])
                            index_fc = j
                            break
                    
                    atom_index = self.findAtomIndex(new_id)
                    if atom_index == None:
                        continue
                    else:
                        tmp_newfc = self.list_of_atoms[atom_index][2]

                    if tmp_oldfc != tmp_newfc:
                        if tmp_oldfc == 0 and tmp_newfc != 0:
                            tmp = "formalCharge=\"" + str(tmp_newfc) + "\""
                            atom = atom[0:len(atom)-1] + [tmp] + [atom[len(atom)-1]]
                        elif tmp_oldfc != 0 and tmp_newfc != 0:
                            tmp = "formalCharge=\"" + str(tmp_newfc) + "\""
                            atom[index_fc] = tmp
                            # atom = atom[0:len(atom)-1] + [tmp] + [atom[len(atom)-1]]
                        else:
                            del atom[index_fc]

                    # Lone pairs
                    tmp_oldlp = 0
                    index_lp = None
                    for j in range(len(atom)):
                        if "lonePair" in atom[j]:
                            tmp_oldlp = int(atom[j].split("lonePair=\"")[1].split("\"")[0])
                            index_lp = j
                            break
                    
                    tmp_newlp = self.list_of_atoms[atom_index][3]

                    if tmp_oldlp != tmp_newlp:
                        if tmp_oldlp == 0 and tmp_newlp != 0:
                            tmp = "lonePair=\"" + str(tmp_newlp) + "\""
                            atom = atom[0:len(atom)-1] + [tmp] + [atom[len(atom)-1]]
                        elif tmp_oldlp != 0 and tmp_newlp != 0:
                            tmp = "lonePair=\"" + str(tmp_newlp) + "\""
                            atom[index_lp] = tmp
                            # atom = atom[0:len(atom)-1] + [tmp] + [atom[len(atom)-1]]
                        else:
                            del atom[index_lp]

                    tmp = ""
                    for j in range(len(atom)):
                        if j != len(atom):
                            tmp += atom[j] + " "
                        else:
                            tmp += atom[j]
                    
                    if tmp[-2:].strip() == "/>" or tmp[-3:].strip() == "/>":
                        content[i] = tmp
                    else:
                        content[i] = tmp + "/>"                                
                
                if "role=\"SruSgroup\"" in content[i]:
                    if update_atom_ids == True:
                            continue
                            # tmp_s = content[i].split("atomRefs=\"")
                            # tmp_atom_ids = tmp_s[1].split("\" cor")[0].split(" ")
                            # tmp_atom_ids = [int(x.replace("a","")) for x in tmp_atom_ids]
                            # tmp_atom_ids = [self.updated_ids[x] for x in tmp_atom_ids]

                            # # Back to string
                            # tmp_atom_ids = ["a" + str(x) for x in tmp_atom_ids]

                            # content[i] = tmp_s[0] + "atomRefs=\"" + " ".join(tmp_atom_ids) + "\" cor" + tmp_s[1].split("\" cor")[1]
                            # content[i] = ""

                if "<MEFlow" in content[i] or "<MAtom" in content[i] or "</MEFlow" in content[i]:
                    if update_atom_ids == True:
                        number_of_changes = content[i].count(".a")
                        
                        if number_of_changes == 0:
                            f.write("%s\n" % content[i])
                            continue
                        elif number_of_changes == 1:
                            tmp = content[i].split(".a")
                            old_id = int(tmp[1].split("\"")[0])
                            new_id = self.updated_ids[old_id]
                            content[i] = tmp[0] + ".a" + str(new_id) + "\"" + tmp[1].split("\"")[1]
                        else: # number_of_changes == 2
                            tmp = content[i].split(".a")
                            old1_id = int(tmp[1].split(" m")[0])
                            old2_id = int(tmp[2].split("\"")[0])
                            new1_id = self.updated_ids[old1_id]
                            new2_id = self.updated_ids[old2_id]

                            content[i] = tmp[0] + ".a" + str(new1_id) + " m" + tmp[1].split(" m")[1][0] + ".a" + str(new2_id) + "\"" + tmp[2].split("\"")[1]

                        f.write("%s\n" % content[i])
                    else:
                        continue
                else:
                    if update_atom_ids == True:
                        if "<MElectron atomRefs=\"m1" in content[i]:
                            tmp = content[i].split("<MElectron atomRefs=\"m1.a")[1]
                            old_id = int(tmp.split("\" dif")[0])                
                            new_id = self.updated_ids[old_id]

                            content[i] = "<MElectron atomRefs=\"m1.a" + str(new_id) + "\" dif" + tmp.split("\" dif")[1]

                    f.write("%s\n" % content[i])
                # Inserted new bonds
                if i == indices[0]:
                    for bond in tmp_bonds:
                        f.write("%s\n" % bond)

    def getBond(self, pair, original):
        if original==False:
            tmp_list = self.list_of_bonds
        else:
            tmp_list = self.list_of_bonds_or

        f_a0 = np.where(tmp_list[:,0] == pair[0])
        s_a0 = np.where(tmp_list[:,1] == pair[0])

        f_a1 = np.where(tmp_list[:,0] == pair[1])
        s_a1 = np.where(tmp_list[:,1] == pair[1])

        intersec1 = np.intersect1d(f_a0, s_a1)
        intersec2 = np.intersect1d(s_a0, f_a1)

        if len(intersec1) != 0:
            index = intersec1[0]
        elif len(intersec2) != 0:
            index = intersec2[0]
        else:
            index = -1
        
        return index

    def getModifiedChanges(self, modified_atoms):
        updated_modified = []
        for i in range(len(modified_atoms)):
            tmp = modified_atoms[i].split("-")

            pair = [int(tmp[0]), int(tmp[1])]

            original_bond = self.getBond(pair, original = True)
            final_bond = self.getBond(pair, original = False)

            if original_bond != -1:
                if self.list_of_bonds_or[original_bond][2] == -5:
                    continue

            if final_bond != -1:
                if self.list_of_bonds[final_bond][2] == -5:
                    continue

            if final_bond == -1:
                changes = 0.0
            else:
                changes = float(self.list_of_bonds[final_bond][2])

            updated_modified.append(modified_atoms[i] + "-" + str(changes))

        return updated_modified

    def transformReac(self):
        tmp_array = copy.deepcopy(self.double_e)

        new_array = []
        for array in tmp_array:
            tmp = []
            for sub_array in array:
                if len(sub_array) == 2:
                    tmp.append(sub_array)
                else:
                    sub_array.append(None)
                    tmp.append(sub_array)

            tmp = np.array(tmp).flatten()
            new_array.append(tmp)

        self.double_e = new_array

    def sortReac(self):
        or_array = self.double_e
        new_array = copy.deepcopy(self.double_e)

        found_array = []
        new_array = np.asarray(new_array)
        for i in range(len(new_array)):
            for j in range(len(new_array[i])):
                tmp = []
                num = new_array[i][j]

                if num == None:
                    continue

                found = np.where(new_array == num)[0]

                if len(set(found)) == 1:
                    continue
                else:
                    other_index = np.where(found != i)[0][0]
                    if j == 0 or j == 1:
                        tmp.append(found[other_index])
                        tmp.append(i)
                    else: # j==2 or j==3
                        tmp.append(i)
                        tmp.append(found[other_index])

                
                if tmp not in found_array:
                    found_array.append(tmp)

        found_array = np.array(found_array)

        # Find start
        for i in range(len(found_array)):
            f = found_array[i][0]
            l = found_array[i][1]
                
            index = np.where(found_array[:,1] == f)[0]
            if len(index) == 0:
                start = i 
                break

        sorted_a = []
        sorted_a.append(found_array[start])
        for i in range(len(or_array)):
            tmp = sorted_a[-1]
            l = tmp[1]
                
            index = np.where(found_array[:,0] == l)[0]

            if len(index) == 0:
                break 
            else:
                sorted_a.append(found_array[index[0]])

        sorted_a = np.array(sorted_a).flatten()
        indexes = np.unique(sorted_a, return_index=True)[1]
        sorted_a = [sorted_a[index] for index in sorted(indexes)]

        diff_array = np.arange(len(self.double_e))
        diff_array = np.setdiff1d(diff_array, sorted_a)

        # Add the difference
        sorted_a = np.concatenate([sorted_a, diff_array])
        new_array = [new_array[index] for index in sorted_a]

        if len(or_array) == len(new_array):
            self.double_e = new_array
        else:
            self.double_e = or_array

    def removeAtom(self, atom_id):
        for i in range(len(self.list_of_atoms)):
            if self.list_of_atoms[i][0] == atom_id:
                del self.list_of_atoms[i]
                break
    
    def getBondsOfAtom(self, atom_id):
        f_b0 = np.where(self.list_of_bonds[:,0] == atom_id)
        s_b1 = np.where(self.list_of_bonds[:,1] == atom_id)
        
        bond_indices = []
        for index in np.concatenate([f_b0[0], s_b1[0]]):
            if self.list_of_bonds[index][2] != -5:
                bond_indices.append(index)
        
        return bond_indices

    def removeBondGivenAtom(self, atom_id):
        to_delete_indices = self.getBondsOfAtom(atom_id)

        # for index in to_delete_indices:
        self.list_of_bonds = np.delete(self.list_of_bonds, to_delete_indices, 0)

    def removeHydrogen(self, indices_to_remove):
        for index in indices_to_remove:

            # Delete from atom list
            self.removeAtom(index)

            # Delete from bond list
            self.removeBondGivenAtom(index)

    def findAtomIndex(self, atom_id):
        for i in range(len(self.list_of_atoms)):
            if self.list_of_atoms[i][0] == atom_id:
                return i
        
        return None

    def updateAtomIds(self):
        nr_of_atoms = len(self.list_of_atoms)

        org_ids = list(np.array(self.list_of_atoms)[:,0])
        org_ids = [int(x) for x in org_ids]
        new_ids = list(range(1,nr_of_atoms+1))

        self.updated_ids = dict(zip(org_ids, new_ids))
        
        for i in range(len(self.list_of_atoms)):
            atom_id = self.list_of_atoms[i][0]
            self.list_of_atoms[i][0] = self.updated_ids[atom_id]
        
        for i in range(len(self.list_of_bonds)):
            atom1_id = self.list_of_bonds[i][0]
            atom2_id = self.list_of_bonds[i][1]
            
            self.list_of_bonds[i][0] = self.updated_ids[atom1_id]
            self.list_of_bonds[i][1] = self.updated_ids[atom2_id]
    
    def initialize(self, fname):
        print(fname)
        self.collectData(fname)
        self.updateReactions()
        self.list_of_atoms_or = copy.deepcopy(self.list_of_atoms)
        self.list_of_bonds = np.asarray(self.list_of_bonds)
        self.list_of_bonds_or = np.copy(self.list_of_bonds)

        self.transformReac()
        try:
            self.sortReac()
        except:
            print("Error")

    def keepOneHydrogen(self, fname, outf):
        print(fname)
        self.collectData(fname)
        self.updateReactions()
        self.list_of_atoms_or = copy.deepcopy(self.list_of_atoms)
        self.list_of_bonds = np.asarray(self.list_of_bonds)
        self.list_of_bonds_or = np.copy(self.list_of_bonds)
        
        self.transformReac()
        try:
            self.sortReac()
        except:
            print("Error")

        reac_atoms = self.getReactingAtoms()
        nr_of_atoms = len(self.list_of_atoms)

        for target_atom_id in range(1,nr_of_atoms+1):
            target_atom_index = self.findAtomIndex(target_atom_id)
            
            if (target_atom_index == None):
                continue    

            neighbors = self.getNeighboringAtoms(target_atom_id)

            # Get the neighboring hydrogens
            hydrogen_ids = []
            for n_id in neighbors:
                n_atom_index = self.findAtomIndex(n_id)
                n_atom_label = self.list_of_atoms[n_atom_index][1]
                if n_atom_label == "H":
                    hydrogen_ids.append(n_id)
            
            h_size = len(hydrogen_ids)
            if h_size == 0 or h_size == 1:
                continue

            diff_h = np.setdiff1d(hydrogen_ids, reac_atoms)

            if len(diff_h) != h_size:
                self.removeHydrogen(diff_h)
            else:
                self.removeHydrogen(hydrogen_ids[1:])

        self.updateAtomIds()
        self.saveReaction(fname, update_atom_ids = True, outfile = outf)
        
    # Update the list of bonds
    def performReaction(self, fname):
        # print(fname)
        self.collectData(fname)
        self.updateReactions()
        self.list_of_atoms_or = copy.deepcopy(self.list_of_atoms)
        self.list_of_bonds = np.asarray(self.list_of_bonds)
        self.list_of_bonds_or = np.copy(self.list_of_bonds)

        # Modify reactions for double electron pushing
        modified_atoms = []

        # Just a place holder
        old_reac = [[], []]
        
        # Sort the self.double_e
        self.transformReac()

        try:
            self.sortReac()
        except:
            print("Error")
        
        for reac in self.double_e:
            skip_c = False

            before = list(filter(None, reac[:2]))
            after = list(filter(None, reac[2:len(reac)]))

            # If a bond is removed
            if len(before) == 2 and len(after) == 1:
                modified_atoms.append(str(before[0]) + "-" + str(before[1]))
                skip_c = self.removeBond(before, after)
            
            #  If a bond is added
            if len(before) == 1 and len(after) == 2:
                modified_atoms.append(str(after[0]) + "-" + str(after[1]))
                skip_c = self.addBond(before, after)

            # If a bond is changed
            if len(before) == 2 and len(after) == 2:
                modified_atoms.append(str(before[0]) + "-" + str(before[1]))
                modified_atoms.append(str(after[0]) + "-" + str(after[1]))
                skip_c = self.changeBond(before, after)

            # # If electron is changed
            # if len(before) == 1 and len(after) == 1:
            #     modified_atoms.append(str(before[0]))
            #     modified_atoms.append(str(after[0]))

            
            self.updateCharge(old_reac, skip_c, before, after)

            old_reac = [before, after]

        self.saveReaction(fname)
        
        return self.getModifiedChanges(modified_atoms)
