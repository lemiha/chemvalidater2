import numpy as np

fname = "Step_macie.008.02.03_new.mrv"
class Update:
    def updateDocument1(self, fname, content, outf):
        atomids, elementtypes, formalcharges, radicals, lonepairs, mrvalias, mrvextraLabel, mrvqueryprops, mrvpseudo, x2, y2 = None, None, None, None, None, None, None, None, None, None, None

        if "atomID" in content[0]:
            atomids = content[0].split("atomID=\"")[1].split("\" ")[0].split(" ")

        if "elementType" in content[0]:
            elementtypes = content[0].split("elementType=\"")[1].split("\" ")[0].split(" ")

        if "formalCharge" in content[0]:
            formalcharges = content[0].split("formalCharge=\"")[1].split("\" ")[0].split(" ")

        if "radical=\"0 " in content[0] or "radical=\"monovalent " in content[0] or "radical=\"divalent1 " in content[0]:
            radicals = content[0].split("radical=\"")[1].split("\" ")[0].split(" ")

        if "lonePair" in content[0]:
            lonepairs = content[0].split("lonePair=\"")[1].split("\" ")[0].split(" ")

        if "mrvAlias" in content[0]:
            mrvalias = content[0].split("mrvAlias=\"")[1].split("\" ")[0].split(" ")

        if "mrvExtraLabel" in content[0]:
            mrvextraLabel = content[0].split("mrvExtraLabel=\"")[1].split("\" ")[0].split(" ")

        if "mrvQueryProps" in content[0]:
            mrvqueryprops = content[0].split("mrvQueryProps=\"")[1].split("\" ")[0].split(" ")

        if "mrvPseudo" in content[0]:
            mrvpseudo = content[0].split("mrvPseudo=\"")[1].split("\" ")[0].split(" ")


        x2 = content[0].split("x2=\"")[1].split("\" ")[0].split(" ")
        y2 = content[0].split("y2=\"")[1].split("\">")[0].split(" ")

        # Add the start of the document
        u_content = []
        u_content.append("<cml>\n")
        u_content.append("<MDocument>\n")
        u_content.append("<MChemicalStruct>\n")
        u_content.append("<molecule molID=\"m1\">\n")
        u_content.append("<atomArray>\n")

        # Add the atoms
        for i in range(len(atomids)):
            tmp_s = '<'
            
            # Atom id
            tmp_s += "atom id=\"" + str(atomids[i]) + "\" "

            # Element type
            tmp_s += "elementType=\"" + str(elementtypes[i]) + "\" "

            # Formal Charge
            if formalcharges != None and formalcharges[i] != "0":
                tmp_s += "formalCharge=\"" + str(formalcharges[i]) + "\" "

            if radicals != None and radicals[i] != "0":
                tmp_s += "radical=\"" + str(radicals[i]) + "\" "

            # Lone Pair
            if lonepairs != None and lonepairs[i] != "0":
                tmp_s += "lonePair=\"" + str(lonepairs[i]) + "\" "

            # mrvAlias
            if mrvalias != None and mrvalias[i] != "0":
                tmp_s += "mrvAlias=\"" + str(mrvalias[i]) + "\" "

            # mrvExtraLabel
            if mrvextraLabel != None and mrvextraLabel[i] != "0":
                tmp_s += "mrvExtraLabel=\"" + str(mrvextraLabel[i]) + "\" "

            # mrvQueryProps
            if mrvqueryprops != None and mrvqueryprops[i] != "0":
                tmp_s += "mrvQueryProps=\"" + str(mrvqueryprops[i]) + "\" "

            # mrvPseudo
            if mrvpseudo != None and mrvpseudo[i] != "0":
                tmp_s += "mrvPseudo=\"" + str(mrvpseudo[i]) + "\" "

            # x2
            tmp_s += "x2=\"" + str(x2[i]) + "\" "

            # y2
            tmp_s += "y2=\"" + str(y2[i]) + "\"/>\n"

            u_content.append(tmp_s)

        u_content.append("</atomArray>\n")

        # Add the bonds
        u_content.append("<bondArray>\n")

        bonds = content[0].split("<bondArray>")[1].split("</bondArray>")[0] + "</bondArray>"

        bonds = bonds.replace("<bondStereo>", "\n<bondStereo>")
        bonds = bonds.replace("</bondStereo></bond>", "</bondStereo>\n</bond>")
        bonds = bonds.replace("></bond>", "/>\n")
        bonds = bonds.replace("</bond><bond", "</bond>\n<bond")
        bonds = bonds.replace("</bond></bond", "</bond>\n</bond")

        u_content.append(bonds)
        u_content.append("\n")
        u_content.append("</molecule>\n")
        u_content.append("</MChemicalStruct>")

        # Gather information for MElectronContainer
        if "<MElectronContainer" in content[0]:
            b_i = content[0].find("<MElectronContainer")
            e_i = content[0].rfind("</MElectronContainer>")
            melectroncontainer = content[0][b_i:e_i] + "</MElectronContainer>"
            melectroncontainer = melectroncontainer.replace("<MElectron ", "\n<MElectron ")
            melectroncontainer = melectroncontainer.replace("</MElectronContainer>", "\n</MElectronContainer>")
            melectroncontainer = melectroncontainer.replace("<MElectronContainer", "\n<MElectronContainer")
            melectroncontainer = melectroncontainer.replace("></MElectron>", "/>")
            u_content.append(melectroncontainer)

        # Gather information for MEFlow
        if "<MEFlow" in content[0]:
            b_i = content[0].find("<MEFlow")
            e_i = content[0].rfind("</MEFlow>")
            meflow = content[0][b_i:e_i] + "</MEFlow>"
            meflow = meflow.replace("<MAtomSetPoint ", "\n<MAtomSetPoint ")
            meflow = meflow.replace("></MAtomSetPoint>", "/>")
            meflow = meflow.replace("</MEFlow>", "\n</MEFlow>")
            meflow = meflow.replace("<MEFlow ", "\n<MEFlow ")
            meflow = meflow.replace("<MEFlowBasePoint", "\n<MEFlowBasePoint")
            meflow = meflow.replace("></MEFlowBasePoint>", "/>")
            u_content.append(meflow)

        u_content.append("\n</MDocument>")
        u_content.append("\n</cml>")

        f_tmp = open(outf,"w+")

        for i in range(len(u_content)):
            if i == 0 and "<cml>" not in u_content[0]:
                f_tmp.write("<cml>\n")
            f_tmp.write(u_content[i])

        f_tmp.close()

    def updateDocument2(self, fname, content, outf):
        u_content = []

        e_i = content[0].rfind("</MChemicalStruct>")
        b_content = content[0][0:e_i] + "</MChemicalStruct>"

        b_content = b_content.replace("<MDocument>", "<MDocument>\n")
        b_content = b_content.replace("<MChemicalStruct>", "<MChemicalStruct>\n")
        b_content = b_content.replace("<molecule molID=\"m1\">", "<molecule molID=\"m1\">\n")
        b_content = b_content.replace("<atomArray>", "<atomArray>\n")
        b_content = b_content.replace("></atom>", "/>\n")

        b_content = b_content.replace("</atomArray>", "</atomArray>\n")
        b_content = b_content.replace("<bondArray>", "<bondArray>\n")
        b_content = b_content.replace("\"></bond>", "\"/>\n")
        b_content = b_content.replace("<bondStereo>", "\n<bondStereo>")
        b_content = b_content.replace("</bondStereo></bond>", "</bondStereo>\n</bond>\n")

        b_content = b_content.replace("</bondArray>", "</bondArray>\n")
        b_content = b_content.replace("</molecule>", "</molecule>\n")
        b_content = b_content.replace("</MChemicalStruct>", "</MChemicalStruct>")

        u_content.append(b_content)

        # Gather information for MElectronContainer
        if "<MElectronContainer" in content[0]:
            b_i = content[0].find("<MElectronContainer")
            e_i = content[0].rfind("</MElectronContainer>")
            melectroncontainer = content[0][b_i:e_i] + "</MElectronContainer>"
            melectroncontainer = melectroncontainer.replace("<MElectron ", "\n<MElectron ")
            melectroncontainer = melectroncontainer.replace("</MElectronContainer>", "\n</MElectronContainer>")
            melectroncontainer = melectroncontainer.replace("<MElectronContainer", "\n<MElectronContainer")
            melectroncontainer = melectroncontainer.replace("></MElectron>", "/>")
            u_content.append(melectroncontainer)

        # Gather information for MEFlow
        if "<MEFlow" in content[0]:        
            b_i = content[0].find("<MEFlow")
            e_i = content[0].rfind("</MEFlow>")
            meflow = content[0][b_i:e_i] + "</MEFlow>"
            meflow = meflow.replace("<MAtomSetPoint ", "\n<MAtomSetPoint ")
            meflow = meflow.replace("></MAtomSetPoint>", "/>")
            meflow = meflow.replace("</MEFlow>", "\n</MEFlow>")
            meflow = meflow.replace("<MEFlow ", "\n<MEFlow ")
            meflow = meflow.replace("<MEFlowBasePoint", "\n<MEFlowBasePoint")
            meflow = meflow.replace("></MEFlowBasePoint>", "/>")
            u_content.append(meflow)

        u_content.append("\n</MDocument>")
        u_content.append("\n</cml>")

        f_tmp = open(outf,"w+")

        for i in range(len(u_content)):
            if i == 0 and "<cml>" not in u_content[0]:
                f_tmp.write("<cml>\n")
            f_tmp.write(u_content[i])

        f_tmp.close()