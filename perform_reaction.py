import sys
import glob
from manipulations import Reaction
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(2)

def convert(fname):
    print(fname)
    global names_failed
    with open(fname) as f:
        content = f.readlines()

    no_headflag = False

    for line in content:
        # Half arrows is removed
        if "headFlags=\"2\"" in line:
            no_headflag = True
            break

    if no_headflag == False:
        r = Reaction()
        try:
            modified_atoms = r.performReaction(fname)

            # Save modified atoms
            tmp_s = ""
            for i in range(len(modified_atoms)):
                if i != len(modified_atoms)-1:
                    tmp_s += modified_atoms[i] + ";"
                else:
                    tmp_s += modified_atoms[i]
            
            mod_f.write(str(fname) + "," + tmp_s + "\n")

        except:
            names_failed.append(fname)

args = sys.argv
files = []
for file in glob.glob(args[1] + "/*.mrv"):
    files.append(file)


files.sort()

if "oneh" in args[1]:
    mod_f = open("modified_atoms_oneh.txt","w+")
else:
    mod_f = open("modified_atoms.txt","w+")

names_failed = []

pool.map(convert, files)
pool.close()
pool.join()

mod_f.close()

if "id" in args[1]:
    f_exclude = open("excluded_files_id.txt","w+")
else:
    f_exclude = open("excluded_files.txt","w+")

for fname in names_failed:
    f_exclude.write(str(fname)  + "\n")

f_exclude.close()