import glob
import numpy
import os

from analysis_methods import Analysis

a = Analysis()
candidate_folder = "Original-model"
original_data = glob.glob("../ML_Data/ExOr/*_all.*")
print(original_data)

candidate_files = []
for dirpath, dirnames, filenames in os.walk("./" + candidate_folder):
    for filename in [f for f in filenames if f.startswith("all.cbond_detailed")]:
        candidate_files.append(os.path.join(dirpath, filename))

candidate_files.sort()
print(candidate_files)

one_h_rc = []
for o_data in original_data:
    for cand_data in candidate_files:
        one_h_rc.append([o_data, cand_data])

models = [["one_h_rc",one_h_rc]]

# Remove redundant lines
for model in models:
    for pair in model[1]:
        a.removeRedudandantLines(pair)

# Find coverage
for model in models:
    print(model[0])
    for pair in model[1]:
        topk, ks = a.coverage(pair[0], pair[1])
        print(ks)
        # a.getStatisticsBonds(pair[0])
        print(str(pair[1])+ "  " +str(topk))