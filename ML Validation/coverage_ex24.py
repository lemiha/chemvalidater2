import glob
import numpy as np
import os

from analysis_methods import Analysis

a = Analysis()
candidate_folder = "Experiment-24"
original_data = glob.glob("../ML_Data/Ex24/*_val.*")
original_data.sort()

candidate_files = []
for dirpath, dirnames, filenames in os.walk("./" + candidate_folder):
    for filename in [f for f in filenames if f.startswith("val.cbond_detailed")]:
        candidate_files.append(os.path.join(dirpath, filename))

candidate_files.sort()

test_models = []
for o_data in original_data:
    tmp_o = o_data.split("/")[-1]
    tmp_o = tmp_o.split("_val")[0]
    print(tmp_o)
    for cand_data in candidate_files:
        if tmp_o in cand_data:
            test_models.append([o_data, cand_data])
            break

models = [["test_models",test_models]]

# Remove redundant lines
for model in models:
    for pair in model[1]:
        a.removeRedudandantLines(pair)

def getName(fname):
    tmp = pair[1].split("/")[-2].split("-")[-1]
    tmp = tmp.split("_")    
    
    if np.isin(['a'], tmp):
        a_type = "\\textbf{Aromatic}"
    elif np.isin(['na'], tmp):
        a_type = "\\textbf{Non-aromatic}"
    
    if np.isin(['p'], tmp):
        p_type = "\\textbf{Processed}"
    elif np.isin(['np'], tmp):
        p_type = "\\textbf{Non-processed}"

    if np.isin(['h'], tmp):
        h_type = "\\textbf{One hydrogen}"
    elif np.isin(['explicit'], tmp):
        h_type = "\\textbf{All hydrogen}"
    else:
        h_type = "\\textbf{All hydrogen}"

    if np.isin(['rc'], tmp):
        m_type = "\\textbf{Reaction center}"
    elif np.isin(['explicit'], tmp):
        m_type = "\\textbf{All molecules}"
    else:
        m_type = "\\textbf{All molecules}"

    return " & ".join([a_type, p_type, h_type, m_type])

# Find coverage
for model in models:
    print("\\renewcommand{\\arraystretch}{1.5}")
    print("\\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}")
    print("\\hline")
    print("\\multicolumn{4}{|c|}{\\textbf{Settings for datasets}} & \\textbf{8} & \\textbf{10} & \\textbf{12} & \\textbf{14} & \\textbf{16} & \\textbf{18} & \\textbf{20} \\\\ \\hline")
    average_bondchanges = []
    for pair in model[1]:
        topk, ks, average20 = a.coverage(pair[0], pair[1])
        average_bondchanges.append(average20)
        topk = np.array(topk)*100
        topk = np.round(topk, 2)

        topk_toprint = [str(x) for x in topk] 
        # a.getStatisticsBonds(pair[0])

        toprint = getName(pair[1])
        # if "Non-processed" not in toprint:
        #     toprint = toprint.replace(", Processed", "")
        print(toprint + " & " + "\\% & ".join(topk_toprint) + "\\% \\\\ \\hline")
    
    average_bondchanges = np.round(np.array(average_bondchanges),2)
    average_bondchanges_str = [str(x) for x in average_bondchanges]
    print("\\% & ".join(average_bondchanges_str) + "\\%")
    print("\\end{tabular}")