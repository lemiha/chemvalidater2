import glob
import numpy
import os

from analysis_methods import Analysis

a = Analysis()
candidate_folder = "Experiment-18"
original_data = glob.glob("../ML_Data/*_val.*")

candidate_files = []
for dirpath, dirnames, filenames in os.walk("./" + candidate_folder):
    for filename in [f for f in filenames if f.startswith("val.cbond_detailed")]:
        candidate_files.append(os.path.join(dirpath, filename))

candidate_files.sort()
print(candidate_files)

one_h_rc = [["../ML_Data/Ex18/one_h_rc_val.txt.proc", "./Experiment-18/model-300-2-direct-one_h_rc/val.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/Ex18/one_h_rc_val.txt.proc", "./Experiment-18/model-300-3-direct-one_h_rc/val.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/Ex18/one_h_rc_val.txt.proc", "./Experiment-18/model-400-2-direct-one_h_rc/val.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/Ex18/one_h_rc_val.txt.proc", "./Experiment-18/model-400-3-direct-one_h_rc/val.cbond_detailed_one_h_rc-40000"]
    ]

models = [["one_h_rc",one_h_rc]]

# Remove redundant lines
for model in models:
    for pair in model[1]:
        a.removeRedudandantLines(pair)

# Find coverage
for model in models:
    print(model[0])
    for pair in model[1]:
        topk, ks = a.coverage(pair[0], pair[1])
        print(ks)
        # a.getStatisticsBonds(pair[0])
        print(str(pair[1])+ "  " +str(topk))