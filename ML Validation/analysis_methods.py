from collections import Counter

class Analysis:
    def removeRedudandantLines(self, f_pair):
        with open(f_pair[0]) as f:
            content_original = f.readlines()

        with open(f_pair[1]) as f:
            content_candidates = f.readlines()

        original_len = len(content_original)
        content_candidates = content_candidates[:original_len]

        f_out = open(f_pair[1], "w+")
        for line in content_candidates:
            f_out.write(line)
        f_out.close()


    def coverage(self, original, candidates):
        original = open(original)
        candidates = open(candidates)

        ks = [8, 10, 12, 14, 16, 18, 20]
        topks = [0 for k in ks]
        tot = 0
        count20 = []
        for line in candidates:
            tot += 1
            cand_bonds = []
            i= 0
            for v in line.split(" ")[1:]:
                i += 1
                if i % 2 == 1:
                    if len(v) == 1:
                        continue
                    x,y,t = v.split('-')
                    cand_bonds.append((int(float(x)),int(float(y)),float(t)))
            
            line = original.readline()
            tmp = line.split()[1]
            original_bonds = []
            for v in tmp.split(';'):
                x,y,t = v.split('-')
                x,y = int(x),int(y)
                x,y = min(x,y), max(x,y)
                original_bonds.append((x, y ,float(t)))

            for i in range(len(ks)):
                if set(original_bonds) <= set(cand_bonds[:ks[i]]):
                    if ks[i] == 20:
                        count20.append(len(original_bonds))
                    # print(ks[i])
                    # print(set(original_bonds))
                    # print(set(cand_bonds[:ks[i]]))
                    # print("----------------")
                    topks[i] += 1.0
        

        return [topk/tot for topk in topks], ks, sum(count20)/len(count20)

    def getStatisticsBonds(self, original):
        original = open(original)
        num_of_changes = []

        tot = 0
        for line in original:
            tmp = line.split()[1]
            original_bonds = []
            for v in tmp.split(';'):
                x,y,t = v.split('-')
                x,y = int(x),int(y)
                x,y = min(x,y), max(x,y)
                original_bonds.append((x, y ,float(t)))

            num_of_changes.append(len(original_bonds))

        count_num = Counter(num_of_changes)
        for key, value in count_num.items():
            print(key, value)
        

