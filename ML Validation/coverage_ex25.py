import glob
import numpy
import os

from analysis_methods import Analysis

a = Analysis()
candidate_folder = "Experiment-25"
original_data = glob.glob("../ML_Data/Ex25/*_val.*")
original_data.sort()

candidate_files = []
for dirpath, dirnames, filenames in os.walk("./" + candidate_folder):
    for filename in [f for f in filenames if f.startswith("val.cbond_detailed")]:
        candidate_files.append(os.path.join(dirpath, filename))

candidate_files.sort()

test_models = []
for o_data in original_data:
    tmp_o = o_data.split("/")[-1]
    tmp_o = tmp_o.split("_val")[0]
    print(tmp_o)
    for cand_data in candidate_files:
        if tmp_o in cand_data:
            test_models.append([o_data, cand_data])
            break

models = [["test_models",test_models]]

# Remove redundant lines
for model in models:
    for pair in model[1]:
        a.removeRedudandantLines(pair)

# Find coverage
for model in models:
    print(model[0])
    for pair in model[1]:
        topk, ks = a.coverage(pair[0], pair[1])
        print(ks)
        # a.getStatisticsBonds(pair[0])
        print(str(pair[1])+ "  " +str(topk))