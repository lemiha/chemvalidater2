import numpy as np
import sys

args = sys.argv

if args[1] == "implicit":
    predict_f = "train.cbond_detailed_implicit"
    data_f = "../gathered_data_implicit.txt"
elif args[1] == "explicit":
    predict_f = "train.cbond_detailed_diffexplicit"
else:
    predict_f = "train.cbond_detailed_400_4_explicit"
    # predict_f = "train.cbond_detailed_400_3_explicit"
    # predict_f = "train.cbond_detailed_explicit"
    data_f = "../gathered_data_explicit.txt"

# Gather data
with open(predict_f) as f:
    predict_content = f.readlines()

with open(data_f) as f:
    data_content = f.readlines()

data_content = [x.strip() for x in data_content]
predict_content = [x.strip() for x in predict_content]

predictions = []
for i in range(len(data_content)):
    tmp = np.array(predict_content[i].split(" ")[1:])
    tmp = np.split(tmp, len(tmp)/2)
    tmp = np.char.split(tmp, sep = "0-")
    
    tmp_array = []
    for r in tmp:
        r[0] = [float(x) for x in r[0]]
        r[1] = [float(x) for x in r[1]]

        tmp_e = [np.array(r[0]), np.array(r[1])]

        tmp_array.append(tmp_e)

    tmp_array = np.array(tmp_array)
    predictions.append(tmp_array)

predictions = np.array(predictions)

real_bonds = []
for i in range(len(data_content)):
    tmp = np.array(data_content[i].split(" ")[1:])
    tmp = np.char.split(tmp , sep = ";")
    tmp = np.char.split(tmp[0], sep = "-")

    tmp_array = []
    for r in tmp:
    
        tmp_e = [float(x) for x in r]

        tmp_e = np.array(tmp_e)

        tmp_array.append(tmp_e)

    real_bonds.append(tmp_array)

real_bonds = np.array(real_bonds)

# print(real_bonds[0])

# Process data
true_count = 0
for i in range(len(real_bonds)):
    p_bonds = predictions[i][:10,0] 

    A = real_bonds[i]
    B = p_bonds

    C = np.array([x for x in set(tuple(x) for x in A) & set(tuple(x) for x in B)])

    if len(C) != 0:
        true_count += 1

print(true_count)
    # isin_array = np.isin(p_bonds, real_bonds[i])

    # for b in isin_array:
    #     print(b)
        
