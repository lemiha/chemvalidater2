import numpy as np
import sys
import os
import os.path
import pandas as pd

args = sys.argv

predict_folder = "Experiment-3"

predict_difffiles = []
for dirpath, dirnames, filenames in os.walk("./" + predict_folder):
    for filename in [f for f in filenames if f.startswith("diff.cbond_detailed_explicit-")]:
        predict_difffiles.append(os.path.join(dirpath, filename))

predict_explfiles = []
for dirpath, dirnames, filenames in os.walk("./" + predict_folder):
    for filename in [f for f in filenames if f.startswith("train.cbond_detailed_explicit-")]:
        predict_explfiles.append(os.path.join(dirpath, filename))

predict_difffiles.sort()
predict_explfiles.sort()

data_f_diff = "../gathered_data_diffexplicit.txt"
data_f_explicit = "../gathered_data_explicit.txt"

hidden_sizes = ["Diff", "Explicit"]
model_sizes = [10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000]
true_df = pd.DataFrame(columns=model_sizes, index=hidden_sizes)

# Gather data
with open(data_f_diff) as f:
    data_content_diff = f.readlines()

with open(data_f_explicit) as f:
    data_content_explicit = f.readlines()

# DIFF
real_bonds = []
for i in range(len(data_content_diff)):
    tmp = np.array(data_content_diff[i].split(" ")[1:])
    tmp = np.char.split(tmp , sep = ";")
    tmp = np.char.split(tmp[0], sep = "-")

    tmp_array = []
    for r in tmp:
    
        tmp_e = [float(x) for x in r]

        tmp_e = np.array(tmp_e)

        tmp_array.append(tmp_e)

    real_bonds.append(tmp_array)

real_bonds = np.array(real_bonds)

for predict_f in predict_difffiles:
    print(predict_f)

    tmp = predict_f.split("Experiment-3")[1].split("diff.c")[0].split("-")
    hidden_size = int(tmp[1])
    m_size = int(predict_f.split("/")[-1].split("-")[-1])

    with open(predict_f) as f:
        predict_content = f.readlines()

    data_content_diff = [x.strip() for x in data_content_diff]
    predict_content = [x.strip() for x in predict_content]

    predictions = []
    for i in range(len(data_content_diff)):
        tmp = np.array(predict_content[i].split(" ")[1:])
        tmp = np.split(tmp, len(tmp)/2)
        tmp = np.char.split(tmp, sep = "0-")
        
        tmp_array = []
        for r in tmp:
            r[0] = [float(x) for x in r[0]]
            r[1] = [float(x) for x in r[1]]

            tmp_e = [np.array(r[0]), np.array(r[1])]

            tmp_array.append(tmp_e)

        tmp_array = np.array(tmp_array)
        predictions.append(tmp_array)

    predictions = np.array(predictions)

    # Process data
    true_count = 0
    for i in range(len(real_bonds)):
        p_bonds = predictions[i][:10,0] 

        A = real_bonds[i]
        B = p_bonds

        C = np.array([x for x in set(tuple(x) for x in A) & set(tuple(x) for x in B)])

        if len(C) != 0:
            true_count += 1

    true_df.at["Diff", m_size] = true_count

# EXPLICIT
real_bonds = []
for i in range(len(data_content_explicit)):
    tmp = np.array(data_content_explicit[i].split(" ")[1:])
    tmp = np.char.split(tmp , sep = ";")
    tmp = np.char.split(tmp[0], sep = "-")

    tmp_array = []
    for r in tmp:
    
        tmp_e = [float(x) for x in r]

        tmp_e = np.array(tmp_e)

        tmp_array.append(tmp_e)

    real_bonds.append(tmp_array)

real_bonds = np.array(real_bonds)

for predict_f in predict_explfiles:
    print(predict_f)

    tmp = predict_f.split("Experiment-3")[1].split("diff.c")[0].split("-")
    hidden_size = int(tmp[1])
    m_size = int(predict_f.split("/")[-1].split("-")[-1])

    with open(predict_f) as f:
        predict_content = f.readlines()

    data_content_explicit = [x.strip() for x in data_content_explicit]
    predict_content = [x.strip() for x in predict_content]

    predictions = []
    for i in range(len(data_content_explicit)):
        tmp = np.array(predict_content[i].split(" ")[1:])
        tmp = np.split(tmp, len(tmp)/2)
        tmp = np.char.split(tmp, sep = "0-")
        
        tmp_array = []
        for r in tmp:
            r[0] = [float(x) for x in r[0]]
            r[1] = [float(x) for x in r[1]]

            tmp_e = [np.array(r[0]), np.array(r[1])]

            tmp_array.append(tmp_e)

        tmp_array = np.array(tmp_array)
        predictions.append(tmp_array)

    predictions = np.array(predictions)

    # Process data
    true_count = 0
    for i in range(len(real_bonds)):
        p_bonds = predictions[i][:10,0] 

        A = real_bonds[i]
        B = p_bonds

        C = np.array([x for x in set(tuple(x) for x in A) & set(tuple(x) for x in B)])

        if len(C) != 0:
            true_count += 1

    true_df.at["Explicit", m_size] = true_count

print(true_df.to_latex())
        
