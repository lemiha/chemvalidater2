import glob
import numpy
import os

from analysis_methods import Analysis

a = Analysis()

candidate_folder = "Experiment-14"
original_data = glob.glob("../ML_Data/*_val.*")

candidate_files = []
for dirpath, dirnames, filenames in os.walk("./" + candidate_folder):
    for filename in [f for f in filenames if f.startswith("train.cbond_detailed")]:
        candidate_files.append(os.path.join(dirpath, filename))

candidate_files.sort()

allh = [["../ML_Data/one_h_val.txt.proc", "./Experiment-14/model-300-3-direct-allh/train.cbond_detailed_one_h-40000"],
    ["../ML_Data/one_h_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-allh/train.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/allh_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-allh/train.cbond_detailed_allh_rc-40000"],
    ["../ML_Data/allh_val.txt.proc", "./Experiment-14/model-300-3-direct-allh/train.cbond_detailed_allh-40000"]]

allh_rc = [["../ML_Data/one_h_val.txt.proc", "./Experiment-14/model-300-3-direct-allh_rc/train.cbond_detailed_one_h-40000"],
    ["../ML_Data/one_h_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-allh_rc/train.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/allh_val.txt.proc", "./Experiment-14/model-300-3-direct-allh_rc/train.cbond_detailed_allh-40000"],
    ["../ML_Data/allh_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-allh_rc/train.cbond_detailed_allh_rc-40000"]]

one_h = [["../ML_Data/one_h_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h/train.cbond_detailed_one_h-40000"],
    ["../ML_Data/one_h_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h/train.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/allh_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h/train.cbond_detailed_allh_rc-40000"],
    ["../ML_Data/allh_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h/train.cbond_detailed_allh-40000"]]

one_h_rc = [["../ML_Data/one_h_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h_rc/train.cbond_detailed_one_h-40000"],
    ["../ML_Data/one_h_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h_rc/train.cbond_detailed_one_h_rc-40000"],
    ["../ML_Data/allh_rc_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h_rc/train.cbond_detailed_allh_rc-40000"],
    ["../ML_Data/allh_val.txt.proc", "./Experiment-14/model-300-3-direct-one_h_rc/train.cbond_detailed_allh-40000"]]

models = [["allh",allh],
        ["allh_rc",allh_rc],
        ["one_h",one_h],
        ["one_h_rc",one_h_rc]]

# Remove redundant lines
for model in models:
    for pair in model[1]:
        a.removeRedudandantLines(pair)

# Find coverage
for model in models:
    print(model[0])
    for pair in model[1]:
        print(pair)
        topk, ks = a.coverage(pair[0], pair[1])
        # a.getStatisticsBonds(pair[0])
        print(str(pair[0])+ "  " +str(topk))