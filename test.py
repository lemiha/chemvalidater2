import subprocess
import numpy as np
from collections import Counter
import copy

or_array = [[[47, 53], [47, 50]], [[47, 48], [48, 53]], [[51, 50], [51, 53]]]

tmp_array = copy.deepcopy(or_array)

new_array = []
for array in tmp_array:
    tmp = []
    for sub_array in array:
        if len(sub_array) == 2:
            tmp.append(sub_array)
        else:
            sub_array.append(None)
            tmp.append(sub_array)

    tmp = np.array(tmp).flatten()
    new_array.append(tmp)

print(new_array)

found_array = []
new_array = np.asarray(new_array)
for i in range(len(new_array)):
    for j in range(len(new_array[i])):
        tmp = []
        num = new_array[i][j]

        if num == None:
            continue

        found = np.where(new_array == num)[0]

        if len(set(found)) == 1:
            continue
        else:
            other_index = np.where(found != i)[0][0]
            if j == 0 or j == 1:
                tmp.append(found[other_index])
                tmp.append(i)
            else: # j==2 or j==3
                tmp.append(i)
                tmp.append(found[other_index])

        
        if tmp not in found_array:
            found_array.append(tmp)

found_array = np.array(found_array)

# Find start
for i in range(len(found_array)):
    f = found_array[i][0]
    l = found_array[i][1]
        
    index = np.where(found_array[:,1] == f)[0]
    if len(index) == 0:
        start = i 
        break

sorted_a = []
sorted_a.append(found_array[start])
for i in range(len(tmp_array)):
    tmp = sorted_a[-1]
    l = tmp[1]
        
    index = np.where(found_array[:,0] == l)[0]

    if len(index) == 0:
        break 
    else:
        sorted_a.append(found_array[index[0]])

sorted_a = np.array(sorted_a).flatten()
indexes = np.unique(sorted_a, return_index=True)[1]
sorted_a = [sorted_a[index] for index in sorted(indexes)]

diff_array = np.arange(len(or_array))
diff_array = np.setdiff1d(diff_array, sorted_a)

# Add the difference
sorted_a = np.concatenate([sorted_a, diff_array])
tmp_array = [tmp_array[index] for index in sorted_a]