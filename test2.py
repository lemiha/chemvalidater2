from rdkit import Chem

tmp_mol = Chem.MolFromSmiles("[H:1][CH2:2][CH:3]([H:4])[c:5]1[n:6][c:7]([H:8])[n:9]([H:10])[c:11]1[H:12].[H:13][CH2:14][CH:15]([H:16])[c:17]1[c:18]([H:19])[n:20]([H:21])[c:22]([H:23])[n+:24]1[H:25].[H:26][O:27][CH:28]([H:29])[C:30]([H:31])([O:32][H:33])[CH:34]([H:35])[O:36][P:37]([O-:38])(=[O:39])[O:40][*:41]", sanitize = False)

tmp_mol.UpdatePropertyCache()
Chem.GetSymmSSSR(tmp_mol)
Chem.Kekulize(tmp_mol)
Chem.SetAromaticity(tmp_mol)

print(Chem.MolToSmiles(tmp_mol))