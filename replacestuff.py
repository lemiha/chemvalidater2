import numpy as np
import sys
import glob

args = sys.argv
files = []
for file in glob.glob(args[1] + "/*.mrv"):
    files.append(file)

files.sort()

for fname in files:
    with open(fname) as f:
        content = f.readlines()

    for i in range(len(content)):

        # Exhange with Uranium
        if "elementType=\"*\"" in content[i]:
            content[i] = content[i].replace("elementType=\"*\"", "elementType=\"U\"")

        if "elementType=\"R\"" in content[i]:
            content[i] = content[i].replace("elementType=\"R\"", "elementType=\"U\"")

        # Remove radicals
        if " radical=\"monovalent\"" in content[i]:
            content[i] = content[i].replace(" radical=\"monovalent\"", "")

        if " radical=\"divalent\"" in content[i]:
            content[i] = content[i].replace(" radical=\"divalent\"", "")

        if " radical=\"divalent1\"" in content[i]:
            content[i] = content[i].replace(" radical=\"divalent1\"", "")
        
        # Remove ANY bond type
        if " queryType=\"Any\"" in content[i]:
            content[i] = content[i].replace(" queryType=\"Any\"", "")

    fout = open(fname, "w+")

    for line in content:
        fout.write(line)
    
    fout.close()

