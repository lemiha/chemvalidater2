import glob
import pandas as pd 

paths_explicit = []
paths_implicit = []

explicit_count = []
implicit_count = []

# Explicit
paths_explicit.append("explicit_data/reactants")
paths_explicit.append("explicit_data/products")
paths_explicit.append("explicit_data/reactants_id")
paths_explicit.append("explicit_data/products_id")
paths_explicit.append("explicit_data/smiles_reactants")
paths_explicit.append("explicit_data/smiles_products")
paths_explicit.append("explicit_data/smiles_reactants_id")
paths_explicit.append("explicit_data/smiles_products_id")

# Implicit
paths_implicit.append("implicit_data/reactants")
paths_implicit.append("implicit_data/products")
paths_implicit.append("implicit_data/reactants_id")
paths_implicit.append("implicit_data/products_id")
paths_implicit.append("implicit_data/smiles_reactants")
paths_implicit.append("implicit_data/smiles_products")
paths_implicit.append("implicit_data/smiles_reactants_id")
paths_implicit.append("implicit_data/smiles_products_id")

def countH2(files):
    h2count = 0
    # for i in reversed(range(len(files))):
    #     with open(files[i]) as f:
    #         content = f.readlines()

    #     for line in content:
    #         if "headFlags=\"2\"" in line:
    #             h2count += 1
    #             break

    return h2count

for path in paths_explicit:
    files = []
    for file in glob.glob(path + "/*"):
        files.append(file)


    h2count = countH2(files)
    
    explicit_count.append([len(files), h2count])

for path in paths_implicit:
    files = []
    for file in glob.glob(path + "/*"):
        files.append(file)

    h2count = countH2(files)

    implicit_count.append([len(files), h2count])

# initialize list of lists 
types = ["reactants", "products", "reactants_id", "products_id", "smiles_reactants", "smiles_products", "smiles_reactants_id", "smiles_products_id"]

data = []
for i in range(len(types)):
    data.append([types[i], 
            explicit_count[i][0], 
            [explicit_count[i][0] - explicit_count[i][1], explicit_count[i][1]], 
            implicit_count[i][0],
            [implicit_count[i][0] - implicit_count[i][1], implicit_count[i][1]]])

# Create the pandas DataFrame 
df = pd.DataFrame(data, columns = ['Type', 'Explixit count', 'H.A. Explicit', 'Implicit Count', 'H.A. Implicit'])

print(df.to_string(index=False))