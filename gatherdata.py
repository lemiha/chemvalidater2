import numpy as np
import sys
import glob
import subprocess

args = sys.argv

subprocess.run(["python3", "compare_smiles.py", "explicit"])

# Reactants smiles files
smiles_reactant_files = []
if "explicit_h" in args[1]:
    for file in glob.glob("explicit_data_oneh" + "/smiles_reactants_id/*.smi"):
        smiles_reactant_files.append(file)
else:
    for file in glob.glob(args[1] + "_data" + "/smiles_reactants_id/*.smi"):
        smiles_reactant_files.append(file)

for i in range(len(smiles_reactant_files)):
    tmp = smiles_reactant_files[i].split(".")
    tmp_f = tmp[1] + "." + tmp[2] + "." + tmp[3][0:2] + "." + tmp[4][:3]
    smiles_reactant_files[i] = [tmp_f, smiles_reactant_files[i]]

smiles_reactant_files = np.array(smiles_reactant_files)


# Products smiles files
smiles_products_files = []
if "explicit_h" in args[1]:
    for file in glob.glob("explicit_data_oneh" + "/smiles_products_id/*.smi"):
        smiles_products_files.append(file)
else:
    for file in glob.glob(args[1] + "_data" + "/smiles_products_id/*.smi"):
        smiles_products_files.append(file)

for i in range(len(smiles_products_files)):
    tmp = smiles_products_files[i].split(".")
    tmp_f = tmp[1] + "." + tmp[2] + "." + tmp[3][0:2] + "." + tmp[4][:3]
    smiles_products_files[i] = [tmp_f, smiles_products_files[i]]

smiles_products_files = np.array(smiles_products_files)

# Load the correct smiles
with open("correct_smiles.txt") as f:
    correct_tmp = f.readlines()
    

correct_tmp = [x.strip() for x in correct_tmp]

correct_files = []
for line in correct_tmp:
    tmp_f = line.split(".")
    tmp_f = tmp_f[1] + "." + tmp_f[2] + "." + tmp_f[3][0:2] + "." + tmp_f[4][:3]
    correct_files.append(tmp_f)

print(len(correct_files))

# Load the modified atoms
if "explicit_h" in args[1]:
    modified_atoms = np.genfromtxt("modified_atoms_oneh.txt", delimiter=",", dtype="U")
else:
    modified_atoms = np.genfromtxt("modified_atoms.txt", delimiter=",", dtype="U")

for i in range(len(modified_atoms)):
    tmp = modified_atoms[i][0].split(".")
    modified_atoms[i][0] = tmp[1] + "." + tmp[2] + "." + tmp[3][0:2] + "." + tmp[4][:3]

# Make the data
data_f = open("gathered_data_" + args[1] + ".txt","w+")
data_fwn = open("gathered_data_" + args[1] + "_withname.txt","w+")
for c_file in correct_files:
    m_index = np.where(modified_atoms[:,0] == c_file)[0][0]
    r_index = np.where(smiles_reactant_files[:,0] == c_file)[0][0]
    p_index = np.where(smiles_products_files[:,0] == c_file)[0][0]

    with open(smiles_reactant_files[r_index,1]) as f:
        reac_smi = f.readline().strip()

    with open(smiles_products_files[p_index,1]) as f:
        prod_smi = f.readline().strip()

    mod = modified_atoms[m_index, 1].strip()

    if reac_smi == "" or prod_smi == "" or mod == "":
        continue
    
    data_fwn.write(c_file + "  " + reac_smi + ">>" + prod_smi + " " + mod + "\n")
    data_f.write(reac_smi + ">>" + prod_smi + " " + mod + "\n")

data_f.close()
data_fwn.close
