from examine_predicted import replaceAtom, getOriginal
import numpy as np
import re
from collections import Counter

org_file = "Nr_of_bonds_data/na_explicit_h_rc_train.txt.proc"

with open(org_file) as f:
    o_content = f.readlines()
o_content = [x.strip() for x in o_content]

changes = []
for smiles in o_content:
    o_edits = getOriginal(smiles)

    pattern_ids = "\:(.*?)\]"
    pattern_atoms = "\[(.*?)\:"
    
    matches_ids = re.findall(pattern_ids, smiles, re.DOTALL)
    matches_ids = [int(x) for x in matches_ids]
    matches_atoms = re.findall(pattern_atoms, smiles, re.DOTALL)

    o_pairs = dict(zip(matches_ids, matches_atoms))

    for o_e in o_edits:
        id1 = int(o_e[0])
        id2 = int(o_e[1])
        atom1 = replaceAtom(o_pairs[id1])
        atom2 = replaceAtom(o_pairs[id2])
        
        tmp = [atom1, atom2]
        tmp.sort()
        # tmp.append(o_e[2])
        changes.append(tmp)

changes = [str(list(x)) for x in changes]
changes_counter = Counter(changes)
for key, count in changes_counter.most_common()[:10]:
    tmp = key.split("', '")
    atom1 = tmp[0].replace("['", "") 
    atom2 = tmp[1].replace("']", "") 

    print("\\textbf{" + str(atom1) + "} & \\textbf{" + str(atom2) + "} & " + str(count) + " & " + str(round(count/len(changes)*100,2)) + "\%")

print(len(changes))