import numpy as np
import re
import sys
from collections import Counter

def getOriginal(osmiles):
    true_edits = osmiles.split(" ")[1].split(";")

    updated_edits = []
    for edit in true_edits:  
        tmp = edit.split("-")

        if len(tmp) == 1:
            continue

        tmp = [int(float(tmp[0])), int(float(tmp[1])), float(tmp[2])]
        
        updated_edits.append([min(tmp[0], tmp[1]), max(tmp[0], tmp[1]), tmp[2]])
    
    true_edits = updated_edits
    return np.array(true_edits)

def getCandidates(smiles):
    tmp = smiles.split(" ")[1:]
    edits = tmp[::2]
    scores = tmp[1::2]
    
    # Reformat edits
    updated_edits = []
    for edit in edits:        
        tmp = edit.split("-")

        if len(tmp) == 1:
            continue

        tmp = [int(float(tmp[0])), int(float(tmp[1])), float(tmp[2])]
        
        updated_edits.append([min(tmp[0], tmp[1]), max(tmp[0], tmp[1]), tmp[2]])

    edits = updated_edits

    # Reformat scores
    scores = [float(x) for x in scores]

    pattern_ids = "\:(.*?)\]"
    pattern_atoms = "\[(.*?)\:"
    
    matches_ids = re.findall(pattern_ids, smiles, re.DOTALL)
    matches_ids = [int(x) for x in matches_ids]
    matches_atoms = re.findall(pattern_atoms, smiles, re.DOTALL)

    pairs = dict(zip(matches_ids, matches_atoms))

    return pairs, np.array(edits), np.array(scores)

def replaceAtom(atom):
    atom = atom.replace("+", "")
    atom = atom.replace("-", "")
    atom = atom.replace("OH", "O")
    atom = atom.replace("SH", "S")
    atom = atom.replace("1", "")
    atom = atom.replace("2", "")
    atom = atom.replace("3", "")
    atom = atom.replace("4", "")
    atom = atom.replace("5", "")
    atom = atom.replace("6", "")

    if "NH+" == atom:
        return "N" 
    elif "NH" == atom:
        return "N" 
    elif "OH" == atom:
        return "O" 
    elif "CH" == atom:
        return "C" 
    elif "NH2" == atom:
        return "N" 
    elif "H+" == atom:
        return "H"
    elif "H-" == atom:
        return "H" 
    elif "NH2+" == atom:
        return "N" 
    elif "CH2" == atom:
        return "C"
    elif "N+" == atom:
        return "N"
    elif "O-" == atom:
        return "O"
    elif "O+" == atom:
        return "O"
    elif "C+" == atom:
        return "C"
    elif "C-" == atom:
        return "C"
    elif "S-" == atom:
        return "S"
    elif "S+" == atom:
        return "S"
    elif "V+" == atom:
        return "V"
    elif "N-" == atom:
        return "N"
    else:
        return atom

org_file = "../ML_Data/Ex25/na_explicit_h_rc_val.txt.proc"
cand_file = "../ML Validation/Experiment-25/model-300-3-direct-na_explicit_h_rc/val.cbond_detailed_na_explicit_h_rc-40000"

# Get original smiles
with open(org_file) as f:
    o_content = f.readlines()
o_content = [x.strip() for x in o_content]

# Get cand smiles
with open(cand_file) as f:
    c_content = f.readlines()
c_content = [x.strip() for x in c_content]


changes = []
for o_smiles, c_smiles in zip(o_content, c_content):
    o_edits = getOriginal(o_smiles)
    c_pairs, c_edits, c_scores = getCandidates(c_smiles)

    o_edits_s = [str(list(x)) for x in o_edits]
    c_edits_s = [str(list(x)) for x in c_edits]
    keep_indices = np.logical_not(np.isin(c_edits_s, o_edits_s))
    
    c_edits = c_edits[keep_indices]
    c_scores = c_scores[keep_indices]
    
    ps_indices = np.where(c_scores > 0.0)[0]
    c_edits = c_edits[ps_indices]
    c_scores = c_scores[ps_indices]

    for c_e in c_edits:
        id1 = int(c_e[0])
        id2 = int(c_e[1])
        atom1 = replaceAtom(c_pairs[id1])
        atom2 = replaceAtom(c_pairs[id2])
        tmp = [atom1, atom2]
        tmp.sort()
        # tmp.append(c_e[2])
        changes.append(tmp)
    
changes = [str(list(x)) for x in changes]

changes_counter = Counter(changes)

for key, count in changes_counter.most_common()[:10]:
    print(key, count, round(count/len(changes)*100,2))

print(len(changes))

    