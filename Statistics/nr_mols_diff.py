import sys
sys.path.insert(1, '../')
from compare_methods import Compare
from collections import Counter
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)

args = sys.argv

def getDiffIndices(rarray, sarray):
    diff_indices = []
    for i in range(len(rarray)):
        left = np.array(sarray[i][0])
        right = np.array(sarray[i][1])

        if len(left) != len(right):
            diff_indices.append(i)

    return diff_indices

def getDiffCount(diff_indices):
    diff_c_left = []
    diff_c_right = []
    for index in diff_indices:
        left = np.array(sarray[index][0])
        right = np.array(sarray[index][1])
            
        tmp_cl = Counter(left)
        tmp_cr = Counter(right)
        
        if len(left) > len(right):
            diff_c_left.append(tmp_cl - tmp_cr)
            
        if len(left) < len(right):    
            diff_c_right.append(tmp_cr - tmp_cl)
            
    return diff_c_left, diff_c_right

def getTotalCount(diff_c_left, diff_c_right):
    total_left = Counter()
    total_right = Counter()

    # Left total count:
    for lc in diff_c_left:
        total_left += lc

    for rc in diff_c_right:
        total_right += rc

    return total_left, total_right

def toLatex(df):



    print("\\begin{tabular}{|l|m{11cm}|r|r|}")
    print("    \\hline")
    print("    \multicolumn{1}{|c|}{\\textbf{}} & \\multicolumn{1}{c|}{\\textbf{Molecule}} & \\multicolumn{1}{c|}{\\textbf{Count}} & \\multicolumn{1}{c|}{\\textbf{P.}} \\\\ \\hline")
    for index, row in df.iterrows():
        print("    \\textbf{" + str(index) +"}  &  \\mintinline[breaklines, fontsize = \\footnotesize]{text}{" + str(row['Molecule']) + "}  &  " + str(row['Count']) + "  &  \\textbf{" + str(row['P.']) + "}  \\\\ \\hline")
        
    print("\\end{tabular}")
    print("")



com = Compare()

# Load array of all possible combinations
rarray, sarray = com.loadData(args, compare_reactants = False)
com.findChargesMismatch(sarray, rarray)
com.examineDiff(sarray, rarray)
com.examineFails(sarray, rarray)

# Get the indices of combinations of the validated data with different sizes
diff_indices = com.getDiffIndices() 

# Get the count for left and right side
diff_c_left, diff_c_right = getDiffCount(diff_indices)

# Get total count left and right side

top_left = 8
top_right = 4
total_left, total_right = getTotalCount(diff_c_left, diff_c_right)
print("Total left:" + str(total_left.most_common(top_left)))
print("Total right:" + str(total_right.most_common(top_right)))

print("Length of left: " + str(len(diff_c_left)))
print("Length of right: " + str(len(diff_c_right)))
print("")

df_left = pd.DataFrame(data=total_left.most_common(top_left), columns=["Molecule", "Count"]).reset_index(drop=True)
df_left.index += 1
df_left['P.'] = round(df_left['Count']/len(diff_c_left), 2)

top = 4
df_right = pd.DataFrame(data=total_right.most_common(top_right), columns=["Molecule", "Count"]).reset_index(drop=True)
df_right.index += 1
df_right['P.'] = round(df_right['Count']/len(diff_c_right), 2)

# Print Latex
print("\\newcommand{\\totalfullreal}{"+ str(len(rarray) - len(com.diff_size)) + "}")
print("\\newcommand{\\totaldiffreal}{"+ str(len(com.diff_size)) + "}")
print("\\newcommand{\\totalequalval}{"+ str(com.count_equal_true) + "}")
print("\\newcommand{\\totaldiffval}{"+ str(len(diff_c_left) + len(diff_c_right)) + "}")
print("\\newcommand{\\totalleft}{"+ str(len(diff_c_left)) + "}")
print("\\newcommand{\\totalright}{"+ str(len(diff_c_right)) + "}")
print("")

toLatex(df_left)
toLatex(df_right)


