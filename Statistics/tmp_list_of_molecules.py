import numpy as np
import glob
from collections import Counter 
import pandas as pd
import re
import matplotlib.pyplot as plt

react_files = []
files = []

for file in glob.glob("../explicit" + "_data" + "/smiles_reactants_id/*.smi"):
    react_files.append(file)

for file in glob.glob("../explicit" + "_data" + "/reactants_id/*.smi"):
    files.append(file)

# Remove Headflag
for i in reversed(range(len(react_files))):
    with open(react_files[i]) as f:
        content = f.readlines()
    
    no_headflag = False

    for line in content:
        if "headFlags=\"2\"" in line:
            del react_files[i]
            break

react_mols = []
for reac in react_files:
    with open(reac) as f:
        content = f.readline()

    content = content.strip()
    # content = content.replace("-", "")
    # content = content.replace("+", "")
    content = content.replace("@", "")

    pattern = "\[(.*?)\:"
    matches = re.findall(pattern, content, re.DOTALL)
    react_mols += matches

atom_counts = Counter(react_mols)


df = pd.DataFrame.from_dict(atom_counts, orient='index').reset_index()
df = df.rename(columns={'index':'Atom', 0:'Count'})
df = df.sort_values('Count', ascending=False)
print(df['Count'].as_matrix()[:10])


# Plot
labels, values = zip(*atom_counts.items())
print(labels)

labels = df['Atom'].as_matrix()[:10]
values = df['Count'].as_matrix()[:10]

indexes = np.arange(len(labels[:10]))
print(indexes)
width = 1

plt.bar(indexes, values, width)
plt.xticks(indexes + width * 0.5, labels)
plt.show()



