import rdkit.Chem as Chem
from collections import Counter
import numpy as np
from collections import defaultdict

'''
This script looks at how many bond changes actually occur in forward reactions (in this dataset)
'''

def getStatisticsBonds(original):
    original = open(original)
    num_of_changes = []

    tot = 0
    for line in original:
        tmp = line.split()[1]
        original_bonds = []
        for v in tmp.split(';'):
            x,y,t = v.split('-')
            x,y = int(x),int(y)
            x,y = min(x,y), max(x,y)
            original_bonds.append((x, y ,float(t)))

        num_of_changes.append(len(original_bonds))

    count_num = Counter(num_of_changes)
    occ_array = []
    for key, value in count_num.items():
        # print(key, value/len(num_of_changes))
        occ_array.append([key, value])

    occ_array = np.array(occ_array)
    occ_array = occ_array[occ_array[:,0].argsort()]
    
    indices = occ_array[:,0]
    percentages = occ_array[:,1]/sum(occ_array[:,1])*100
    percentages_rest = np.round(sum(percentages[10:]),2)
    cum_percentages = np.cumsum(percentages)

    # Round up
    percentages = np.round(percentages,2)
    cum_percentages = np.round(cum_percentages,2)

    # To print
    indices_str = [str(x) for x in indices]
    percentages_str = [str(x) for x in percentages]
    cum_percentages_str = [str(x) for x in cum_percentages]

    if len(indices) < 10:
        print(" & \\textbf{" + "} & \\textbf{".join(indices_str) + "}")
        print(" & " + "\\% & ".join(percentages_str) + "\\%")
        print(" & " + "\\% & ".join(cum_percentages_str) + "\\%")
    else:
        cum_percentages_rest = 100

        print(" & \\textbf{" + "} & \\textbf{".join(indices_str[:10]) + "}" + " & " + "\\textbf{12-24}" )
        print(" & " + "\\% & ".join(percentages_str[:10]) + "\\%" + " & " + str(percentages_rest) + "\\%")
        print(" & " + "\\% & ".join(cum_percentages_str[:10]) + "\\%" + " & " + str(cum_percentages_rest) + "\\%")




getStatisticsBonds("Nr_of_bonds_data/all_rexgen.txt.proc")
print("------------------")
getStatisticsBonds("Nr_of_bonds_data/gathered_data_explicit.txt")