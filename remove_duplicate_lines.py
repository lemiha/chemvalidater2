import sys

args = sys.argv

in_fname = args[1]
out_fnames = args[2:]

with open(in_fname) as f:
    in_content = f.readlines()

in_content = [x.strip() for x in in_content]
lines_seen = set() # holds lines already seen
lines_seen_indices = []

for i in range(len(in_content)):
    if in_content[i] not in lines_seen: # not a duplicate
        lines_seen_indices.append(i)
        lines_seen.add(in_content[i])

# Write the new lines
for out_fname in out_fnames:
    with open(out_fname) as f:
        content = f.readlines()
    
    content = [x.strip() for x in content]

    out_fname = out_fname.split(".txt")[0] + "_rd" + ".txt.proc"
    print(out_fname)
    out_newf = open(out_fname, "w+")

    for i in range(len(content)):
        if i in lines_seen_indices:
            out_newf.write(content[i] + "\n")
        else:
            continue

    out_newf.close()
