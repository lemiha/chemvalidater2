import numpy as np
import glob
from collections import Counter

files = []
for file in glob.glob("Fixed_Data" + "/*.mrv"):
    files.append(file)
files.sort()


entries = []
for f in files:
    entry = f.split("/")[1].split(".")[1]
    entries.append(entry)

c = Counter(entries)
print(c)

entries = np.array(entries)
entries = np.unique(entries)
print(len(entries))
print(entries)