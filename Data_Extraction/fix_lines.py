import glob
import subprocess

files = []
for file in glob.glob("Fixed_Data" + "/*.mrv"):
    files.append(file)

files.sort()
# print(files)

# for fname in files:
    
for fname in files:
    with open(fname, 'r') as file:
        data = file.read().replace('\n', '')

    data = data.split(">")
    # print(data[-2])
    # print(fname)
    f= open(fname,"w+")
    for i in range(len(data)):
        if "</cml" in data[i]:
            f.write(data[i] + ">")
            break
        elif "<bondStereo" in data[i]:
            f.write(data[i] + ">")
        else:
            f.write(data[i] + ">\n")
    f.close()

# print(data)