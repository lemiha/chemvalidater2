import glob
import subprocess
import os
import numpy as np
from shutil import copyfile

files = []
for file in glob.glob("Manuel/Data (in order)" + "/*.mrv"):
    files.append(file)
files.sort()

numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
entries = []

for fname in files:
    if "CSA__entry" in fname:
        # CSA__entry_199_mechanism_1_step_3_jTpRYZB
        tmp = fname.split("_")
        entry = tmp[3]
        mechanism = tmp[5]
        step = tmp[7].split(".mrv")[0]
        
        entries.append([fname, entry, mechanism, step])

    elif "csa_entry" in fname:
        # csa_entry_535_mechanism_3_step_2_qrmFLYg
        tmp = fname.split("_")
        entry = tmp[2]
        mechanism = tmp[4]
        step = tmp[6].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])

    elif "gemma_entry" in fname:
        # gemma_entry_10_mechanism_1_step_1_KGB13p1
        tmp = fname.split("_")
        entry = tmp[2]
        mechanism = tmp[4]
        step = tmp[6].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])

    elif "Gemma_Holliday_entry" in fname:
        # Gemma_Holliday_entry_19_mechanism_1_step_1_f1Lr153
        tmp = fname.split("_")
        entry = tmp[3]
        mechanism = tmp[5]
        step = tmp[7].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])

    elif "Gemma_L._Holliday_entry" in fname:
        # Gemma_L._Holliday_entry_50_mechanism_1_step_2.mrv
        tmp = fname.split("_")
        entry = tmp[4]
        mechanism = tmp[6]
        step = tmp[8].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])
    elif "M-CSA__entry" in fname:
        # M-CSA__entry_163_mechanism_2_step_2
        tmp = fname.split("_")
        entry = tmp[3]
        mechanism = tmp[5]
        step = tmp[7].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])

    elif "MACiE__entry" in fname:
        # MACiE__entry_152_mechanism_3_step_2
        tmp = fname.split("_")
        entry = tmp[3]
        mechanism = tmp[5]
        step = tmp[7].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])

    elif "macie_entry" in fname:
        # macie_entry_4_mechanism_2_step_3_uNXRVM2
        tmp = fname.split("_")
        entry = tmp[2]
        mechanism = tmp[4]
        step = tmp[6].split(".mrv")[0]

        entries.append([fname, entry, mechanism, step])
    else:
        # Step_macie.1.1.3_u4LwyX2
        tmp = fname.split(".")
        entry = tmp[1]
        mechanism = tmp[2]
        step = tmp[3]

        if len(step) == 1:
            step = step[0]
        elif step[1] in numbers:        
            step = step[:2]
        else:
            step = step[0]

        entries.append([fname, entry, mechanism, step])



d_indices = []
for i in range(len(entries)):
    to_check = entries[i][1:4]
    
    for j in range(i+1,len(entries)):
        # if j == i:
        #     continue

        if to_check == entries[j][1:4]:
            # print("no match")
            d_indices.append([i,j])

d_indices = np.array(d_indices)

for tmp in entries:
    # print(tmp[0])
    fname = tmp[0].split("/")
    entry = tmp[1]
    mechanism = tmp[2]
    step = tmp[3]

    if ("a" in entry) or ("b" in entry) or ("c" in entry):
        if len(entry) == 2:
            entry = "000" + entry
        elif len(entry) == 3:
            entry = "00" + entry
        elif len(entry) == 4:
            entry = "0" + entry
    else:    
        if len(entry) == 1:
            entry = "000" + entry
        elif len(entry) == 2:
            entry = "00" + entry
        elif len(entry) == 3:
            entry = "0" + entry

    if len(mechanism) == 1:
        mechanism = "0" + mechanism

    if len(step) == 1:
        step = "0" + step

    src = tmp[0]
    dst = "Fixed_Data" + "/" + "Step_macie." + entry + "." + mechanism + "." + step + ".mrv"
    # print(dst)

    copyfile(src, dst)