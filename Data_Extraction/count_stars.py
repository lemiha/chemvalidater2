import numpy as np
import glob
from collections import Counter

files = []
for file in glob.glob("Fixed_Data" + "/*.mrv"):
    files.append(file)
files.sort()


srars = []
for f in files:
    entry = f.split("/")[1].split(".")[4]
    srars.append(entry)

c = Counter(srars)
print(c)

srars = np.array(srars)
srars = np.unique(srars)
print(len(srars))
print(srars)