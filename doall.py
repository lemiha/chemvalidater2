import subprocess
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(2)

def convert(src):
    subprocess.run(src)

# subprocess.run(["python3", "Data_Extraction/edit_fnames.py"])
# subprocess.run(["python3", "Data_Extraction/fix_lines.py"])

# subprocess.run(["python3", "doremove.py"])

# 1. Create the nesseary folders if they doesn't exist
print(1)
subprocess.run(["python3", "makefolders.py"])

# # 2. Make two datasets. One where the hydrogen is implicit and one where it is explicit 
# print(2)
# subprocess.run(["python3", "makedata.py", "implicit"])
# subprocess.run(["python3", "makedata.py", "explicit"])

# # 3. The generated explicit marvin files are not in the same structure as the files from the M-CSA database, hence it is converted to the same structure
# print(3)
# subprocess.run(["python3", "converttomcsa.py"])

# # 4. Generate the products/output of the mechanistic steps
# print(4)
# # subprocess.run(["python3", "perform_reaction.py", "implicit_data/reactants"])
# subprocess.run(["python3", "perform_reaction.py", "explicit_data/reactants"])

# # 5. Assing identifiers to each atom
# print(5)
# # subprocess.run(["python3", "update_mrv.py", "implicit_data/reactants"])
# # subprocess.run(["python3", "update_mrv.py", "implicit_data/products"])
# subprocess.run(["python3", "update_mrv.py", "explicit_data/reactants"])
# subprocess.run(["python3", "update_mrv.py", "explicit_data/products"])

# 6. Replace "*" and "R" groups with U (are replaced later)
print(6)
# subprocess.run(["python3", "replacestuff.py", "implicit_data/reactants"])
# subprocess.run(["python3", "replacestuff.py", "implicit_data/reactants_id"])
# subprocess.run(["python3", "replacestuff.py", "implicit_data/products"])
# subprocess.run(["python3", "replacestuff.py", "implicit_data/products_id"])

subprocess.run(["python3", "replacestuff.py", "explicit_data/reactants"])
subprocess.run(["python3", "replacestuff.py", "explicit_data/reactants_id"])
subprocess.run(["python3", "replacestuff.py", "explicit_data/products"])
subprocess.run(["python3", "replacestuff.py", "explicit_data/products_id"])

# 7. Convert the Marvin files to SMILES
print(7)
commands = []
# commands.append(["python3", "convert_to_smiles.py", "implicit_data/reactants", "with_stereo"])
# commands.append(["python3", "convert_to_smiles.py", "implicit_data/reactants_id", "with_stereo"])
# commands.append(["python3", "convert_to_smiles.py", "implicit_data/products", "with_stereo"])
# commands.append(["python3", "convert_to_smiles.py", "implicit_data/products_id", "with_stereo"])

commands.append(["python3", "convert_to_smiles.py", "explicit_data/reactants", "with_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data/reactants_id", "with_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data/products", "with_stereo"])
commands.append(["python3", "convert_to_smiles.py", "explicit_data/products_id", "with_stereo"])

pool.map(convert, commands)
pool.close()
pool.join()

# 8 Replace the U with "*" in the SMILES
print(8)
# subprocess.run(["python3", "replacewithstar_and_oh.py", "implicit_data/smiles_reactants"])
# subprocess.run(["python3", "replacewithstar_and_oh.py", "implicit_data/smiles_reactants_id"])
# subprocess.run(["python3", "replacewithstar_and_oh.py", "implicit_data/smiles_products"])
# subprocess.run(["python3", "replacewithstar_and_oh.py", "implicit_data/smiles_products_id"])

subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data/smiles_reactants"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data/smiles_reactants_id"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data/smiles_products"])
subprocess.run(["python3", "replacewithstar_and_oh.py", "explicit_data/smiles_products_id"])

# 9 Gather Data to be used with Rexgen
print(9)
subprocess.run(["python3", "gatherdata.py", "explicit"])
# subprocess.run(["python3", "gatherdata.py", "implicit"])

# # 10 Remove the Reaction SMILES, which cannot be processed by RDkit
# print(10)
# subprocess.run(["python3", "validate_molecules.py"])

# # 11 Print the different fail categories to PDF
# print(11)
# subprocess.run(["python3", "toprint_explicit.py", "fails_coor.txt"])
# subprocess.run(["python3", "tolatex_explicit.py", "fails_coor.txt"])

# subprocess.run(["python3", "toprint_explicit.py", "fails_uncategorised.txt"])
# subprocess.run(["python3", "tolatex_explicit.py", "fails_uncategorised.txt"])

# subprocess.run(["python3", "toprint_explicit.py", "fails_bond.txt"])
# subprocess.run(["python3", "tolatex_explicit.py", "fails_bond.txt"])

# subprocess.run(["python3", "toprint_explicit.py", "fails_charge.txt"])
# subprocess.run(["python3", "tolatex_explicit.py", "fails_charge.txt"])

# subprocess.run(["python3", "toprint_explicit.py", "fails_empthy.txt"])
# subprocess.run(["python3", "tolatex_explicit.py", "fails_empthy.txt"])

# 0043.01.03 0107.03.03
# BACK: