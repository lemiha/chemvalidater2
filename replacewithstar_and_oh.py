import numpy as np
import sys
import glob

args = sys.argv
files = []
for file in glob.glob(args[1] + "/*.smi"):
    files.append(file)

files.sort()

for fname in files:
    with open(fname) as f:
        content = f.readlines()

    for i in range(len(content)):
        if "[U" in content[i]:
            content[i] = content[i].replace("[U", "[*")

        if "oneh" not in args[1]:
            if "[OH:" in content[i]:
                content[i] = content[i].replace("[OH:", "[O:")

    fout = open(fname, "w+")

    for line in content:
        fout.write(line)
    
    fout.close()